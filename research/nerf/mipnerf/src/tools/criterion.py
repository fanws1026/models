# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
"""Loss functions for NeRF model."""

import mindspore as ms
import mindspore.nn as nn


class NerfL2Loss(nn.Cell):

    def __init__(self, coarse_loss_mult: float,
                 clip_loss_max: float = 1000.0):
        super().__init__()
        self.reduce_mean = ms.ops.ReduceMean()
        self.pow = ms.ops.Pow()
        self.coarse_loss_mult = coarse_loss_mult
        self.clip_loss_max = clip_loss_max

    def construct(self, out, target):
        loss_coarse = self.reduce_mean(self.pow((out[:, :3] - target), 2))
        loss_coarse = ms.ops.clip_by_value(
            loss_coarse,
            clip_value_min=ms.Tensor(0.0),
            clip_value_max=ms.Tensor(1000.0)
        )
        loss_fine = self.reduce_mean(self.pow((out[:, 3: 6] - target), 2))
        loss_fine = ms.ops.clip_by_value(
            loss_fine,
            clip_value_min=ms.Tensor(0.0),
            clip_value_max=ms.Tensor(1000.0)
        )
        return loss_coarse * self.coarse_loss_mult + loss_fine

# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
#
# This file or its part has been derived from the following repository
# and modified: https://github.com/bmild/nerf
# ============================================================================
"""Export 3d mesh: useful for blender dataset with synthetic scenes."""

import argparse
import json
from pathlib import Path

import mcubes
import trimesh

import numpy as np
from tqdm import tqdm

import mindspore as ms
from mindspore import ops
from mindspore import load_checkpoint, load_param_into_net

from src.models.nerf_system import NeRFSystem

DEFAULT_MODEL_CONFIG = Path('src') / 'configs' / 'nerf_config.json'


def get_opts():
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--train-config', type=str,
                        required=True,
                        help='Path to the train config.')
    parser.add_argument('--model-config', type=str,
                        default=DEFAULT_MODEL_CONFIG,
                        help='Volume rendering model config.')
    parser.add_argument('--model-ckpt', type=str, required=True,
                        help='Model checkpoints.')
    parser.add_argument('--points', type=int, default=256,
                        help='Number of points for each axis for mesh.')
    parser.add_argument('--batch', type=int, default=4096,
                        help='Points to pass to the fine model.')
    parser.add_argument('--out-stl', type=str, required=True,
                        help='Path for output data saving:'
                             'predicted image, configs, ')
    parser.add_argument('--mode', choices=['graph', 'pynative'],
                        default='graph',
                        help='Model representation mode. '
                             'Pynative for debugging.')
    return parser.parse_args()


def main():
    args = get_opts()

    mode = ms.GRAPH_MODE if args.mode == 'graph' else ms.PYNATIVE_MODE
    ms.set_context(mode=mode)
    with open(args.train_config, 'r') as f:
        train_cfg = json.load(f)
    with open(args.model_config, 'r') as f:
        model_cfg = json.load(f)

    model = NeRFSystem(train_config=train_cfg,
                       nerf_config=model_cfg)

    param_dict = load_checkpoint(args.model_ckpt)
    load_param_into_net(model, param_dict)

    # Build linspace and make the prediction.
    pts_min, pts_max = -1.2, 1.2
    t = np.linspace(pts_min, pts_max, args.points + 1)
    query_pts = np.stack(np.meshgrid(t, t, t), -1).astype(np.float32)
    flat_query_pts = ms.Tensor(query_pts.reshape([-1, 3]), dtype=ms.float32)
    viewdirs = ms.numpy.zeros_like(flat_query_pts, dtype=ms.float32)

    # Predict using the coarse scene representation.
    pos_embedder = model.embedding_xyz
    dir_embedder = model.embedding_dir
    nerf_layer = model.network.inference_fine.model
    chunk = train_cfg["chunk"]
    raw = []
    step = args.batch
    tile = ops.Tile()
    reshape = ops.Reshape()
    concat = ops.Concat(axis=-1)
    concat_axis_0 = ops.Concat(axis=0)
    for i in tqdm(range(0, flat_query_pts.shape[0], step)):

        positions = flat_query_pts[i: i + step, None, :]
        a_embedded = ops.zeros((positions.shape[0], train_cfg["n_a"]),
                               dtype=ms.float32)
        directions = viewdirs[i: i + step]
        pos_shape = positions.shape
        positions = reshape(positions, (-1, pos_shape[-1]))
        positions = pos_embedder.embed(positions)

        # Direction embedding.
        tiled_dir = tile(directions[:, None, :],
                         (1, pos_shape[1], 1))
        directions = reshape(tiled_dir, (-1, directions.shape[-1]))
        directions = dir_embedder.embed(directions)

        # Concat embeddings and apply model.
        embedded = concat((positions, directions))
        embedded = concat((embedded, a_embedded))

        # Raise MS Error:
        res = nerf_layer(embedded[:chunk], output_random=False)
        if chunk < embedded.shape[0]:
            for j in range(chunk, embedded.shape[0], chunk):
                res = concat_axis_0(
                    (res, nerf_layer(embedded[j: j + chunk],
                                     output_random=False))
                )
        output_shape = tuple(list(pos_shape[:-1]) + [4])
        res = reshape(res, output_shape)
        res = res.asnumpy()
        raw.append(res)
    raw = np.concatenate(raw, 0)
    raw = np.reshape(np.array(raw), list(query_pts.shape[:-1]) + [-1])
    sigma = np.maximum(raw[..., -1], 0.0)

    # Generate mesh.
    threshold = 50.0
    vertices, triangles = mcubes.marching_cubes(sigma, threshold)
    mesh = trimesh.Trimesh(vertices / args.points - 0.5, triangles)
    mesh.export(file_obj=args.out_stl, file_type='stl')


if __name__ == '__main__':
    main()

# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
import argparse

import numpy as np
import mindspore as ms
import torch


def parse_args():
    parser = argparse.ArgumentParser(__doc__)
    parser.add_argument('--input')
    parser.add_argument('--output')

    return parser.parse_args()


def build_tree(param):
    result = {}
    for key in param:
        names = key.split('.')
        block = result
        for i, name in enumerate(names):
            if name not in block:
                if i == len(names) - 1:
                    block[name] = param[key]
                else:
                    block[name] = {}
            block = block[name]

    return result


def collect(result, name, tree):
    for key in tree:
        do_pop = True
        if isinstance(tree[key], dict):
            if 'rpn' in key and key != 'rpn_head':
                name.append('rpn_convs_layer.' + key)
            elif key == 'roi_head':
                do_pop = False
            else:
                name.append(key)
            collect(result, name, tree[key])
        else:
            if key == 'num_batches_tracked':
                continue
            if key == 'weight':
                if 'num_batches_tracked' in tree:
                    name.append('gamma')
                else:
                    name.append(key)
            elif key == 'bias':
                if 'num_batches_tracked' in tree:
                    name.append('beta')
                else:
                    name.append(key)
            elif key == 'running_mean':
                name.append('moving_mean')
            elif key == 'running_var':
                name.append('moving_variance')
            else:
                name.append(key)

            full_name = '.'.join(name)

            data = tree[key].cpu().numpy()
            if 'fc_cls' in full_name:
                data0 = data[-1:]
                data1 = data[:-1]
                data = np.concatenate([data0, data1], axis=0)

            result.append({
                'name': full_name,
                'data': ms.Parameter(data, name=full_name)
            })
        print(name)
        if do_pop:
            name.pop()


def main():
    args = parse_args()
    param = torch.load(args.input)['state_dict']

    weight_tree = build_tree(param)

    result = []
    collect(result, [], tree=weight_tree)
    print(result)
    ms.save_checkpoint(result, args.output)


if __name__ == '__main__':
    main()

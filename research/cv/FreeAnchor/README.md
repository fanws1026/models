# Contents

* [Contents](#contents)
    * [FreeAnchor Description](#freeanchor-description)
        * [Model Architecture](#model-architecture)
        * [Dataset](#dataset)
    * [Environment Requirements](#environment-requirements)
    * [Quick Start](#quick-start)
        * [Prepare the model](#prepare-the-model)
        * [Run the scripts](#run-the-scripts)
    * [Script Description](#script-description)
        * [Script and Sample Code](#script-and-sample-code)
        * [Script Parameters](#script-parameters)
    * [Training](#training)
        * [Training Process](#training-process)
        * [Transfer Training](#transfer-training)
        * [Distribute training](#distribute-training)
    * [Evaluation](#evaluation)
        * [Evaluation Process](#evaluation-process)
            * [Evaluation on GPU](#evaluation-on-gpu)
        * [Evaluation result](#evaluation-result)
    * [Inference](#inference)
        * [Inference Process](#inference-process)
            * [Inference on GPU](#inference-on-gpu)
        * [Inference result](#inference-result)
   * [Metrics](#metrics)
   * [Model Description](#model-description)
        * [Performance](#performance)
   * [Description of Random Situation](#random-situation)
   * [ModelZoo Homepage](#modelzoo-homepage)

## [FreeAnchor Description](#contents)

Modern CNN-based object detectors assign anchors for ground-truth objects under
the restriction of object-anchor Intersection-over-Unit (IoU). In this study,
we propose a learning-to-match approach to break IoU restriction, allowing
objects to match anchors in a flexible manner. Our approach, referred to as
FreeAnchor, updates hand-crafted anchor assignment to "free" anchor matching
by formulating detector training as a maximum likelihood estimation (MLE)
procedure. FreeAnchor targets at learning features which best explain a class
of objects in terms of both classification and localization. FreeAnchor is
implemented by optimizing detection customized likelihood and can be fused
with CNN-based detectors in a plug-and-play manner. Experiments on COCO
demonstrate that FreeAnchor consistently outperforms their counterparts with
significant margins.

[Paper](https://arxiv.org/abs/1909.02466): Xiaosong Zhang, Fang Wan, Chang Liu,
Rongrong Ji, Qixiang Ye. Computer Vision and Pattern
Recognition (CVPR), 2019 (In press).

### [Model Architecture](#contents)

**Overview of the pipeline of FreeAnchor:**
FreeAnchor, updates hand-crafted anchor assignment to "free" anchor matching
by formulating detector training as a maximum likelihood estimation (MLE)
procedure.

FreeAnchor result prediction pipeline:

1. Backbone (ResNet, ResNeXt).
2. Neck (FPN).
3. RPN.
4. FreeAnchorRetinaHead.
5. multiclass NMS (reduce number of proposed boxes and omit objects with low
 confidence).

FreeAnchor result training pipeline:

1. Backbone (ResNet, ResNeXt).
2. Neck (FPN + BFP).
3. Assigner.
4. FreeAnchorRetinaHead.
5. Classification + Localization losses.

### [Dataset](#contents)

Note that you can run the scripts based on the dataset mentioned in original
paper or widely used in relevant domain/network architecture. In the following
sections, we will introduce how to run the scripts using the related dataset
below.

Dataset used: [COCO-2017](https://cocodataset.org/#download)

* Dataset size: 25.4G
    * Train: 18.0G，118287 images
    * Val: 777.1M，5000 images
    * Test: 6.2G，40670 images
    * Annotations: 474.7M, represented in 3 JSON files for each subset.
* Data format: image and json files.
    * Note: Data will be processed in dataset.py

## [Environment Requirements](#contents)

* Install [MindSpore](https://www.mindspore.cn/install/en).
* Download the dataset COCO-2017.
* Install third-parties requirements:

```text
Cython~=0.28.5
matplotlib
numpy~=1.21.2
opencv-python~=4.5
pycocotools~=2.0
pyyaml~=6.0.1
scipy~=1.7
seaborn~=0.12
tqdm~=4.64
```

* We use COCO-2017 as training dataset in this example by default, and you
 can also use your own datasets. Dataset structure:

```shell
.
└── coco-2017
    ├── train
    │   ├── data
    │   │    ├── 000000000001.jpg
    │   │    ├── 000000000002.jpg
    │   │    └── ...
    │   └── labels.json
    ├── validation
    │   ├── data
    │   │    ├── 000000000001.jpg
    │   │    ├── 000000000002.jpg
    │   │    └── ...
    │   └── labels.json
    └── test
        ├── data
        │    ├── 000000000001.jpg
        │    ├── 000000000002.jpg
        │    └── ...
        └── labels.json
```

* Also we make evaluation only on mindrecord converted dataset. Use the
 `convert_dataset.py` script to convert original COCO subset to mindrecord.

```shell
python convert_dataset.py --config_path default_config.yaml --converted_coco_path data/coco-2017/validation --converted_mindrecord_path data/mindrecord/validation
```

Result mindrecord dataset will have next format:

```shell
.
└── mindrecord
    ├── validation
    │   ├── file.mindrecord
    │   ├── file.mindrecord.db
    │   └── labels.json
    └── test
        ├── file.mindrecord
        ├── file.mindrecord.db
        └── labels.json
```

It is possible to convert train dataset to mindrecord format and use it to
train model.

## [Quick Start](#contents)

### [Prepare the model](#contents)

1. Prepare yaml config file. Create file and copy content from
 `default_config.yaml` to created file.
1. Change data settings: experiment folder (`train_outputs`), image size
 settings (`img_width`, `img_height`, etc.), subsets folders (`train_dataset`,
 `val_dataset`, `train_data_type`, `val_data_type`), information about
 categories etc.
1. Change the backbone settings: type (`backbone.type`), path to pretrained
 ImageNet weights (`backbone.pretrained`), layer freezing settings
 (`backbone.frozen_stages`).
1. Change other training hyper parameters (learning rate, regularization,
 augmentations etc.).
1. Prepare pre-trained checkpoints.

### [Run the scripts](#contents)

After installing MindSpore via the official website, you can start training and
evaluation as follows:

* running on GPU

```shell
# standalone training on GPU
bash scripts/run_standalone_train_gpu.sh [CONFIG_PATH]

# run eval on GPU
bash scripts/run_eval_gpu.sh [CONFIG_PATH] [CHECKPOINT_PATH]
```

## [Script Description](#contents)

### [Script and Sample Code](#contents)

```shell
FreeAnchor
├── configs
│   ├── retinanet_free_anchor_r101_fpn_1x_coco.yaml        ## ResNet50 config
│   ├── retinanet_free_anchor_r50_fpn_1x_coco.yaml         ## ResNet101 config
│   └── retinanet_free_anchor_x101_32x4d_fpn_1x_coco.yaml  ## ResNeXt50 config
├── convert_dataset.py                     ## Script to convert dataset to mindrecord format
├── convert_weights.py                     ## Script to convert checkpoint to mindspore format
├── default_config.yaml                    ## Default configuration file
├── eval.py                                ## Eval script (ckpt)
├── export.py                              ## Script to convert checkpoint to ONNX format
├── NOTICE
├── predict.py
├── README.md                              ## Descriptions about FreeAnchor
├── requirements.txt                       ## List of requirements
├── scripts
│   ├── run_eval_gpu.sh                    ## Bash script for eval on gpu
│   └── run_standalone_train_gpu.sh        ## Bash script for distributed on gpu
├── src
│   ├── __init__.py
│   ├── data
│   │   ├── __init__.py
│   │   ├── config.py                      ## Configuration file parsing utils
│   │   ├── dataset.py                     ## Images loading and preprocessing
│   │   ├── detecteval.py                  ## DetectEval class to analyze predictions
│   │   └── eval_utils.py                  ## evaluation metrics
│   ├── models
│   │   ├── __init__.py
│   │   └── freeanchor                     ## Full model with implemented pipeline
│   │       ├── __init__.py
│   │       ├── freeanchor.py
│   │       └── layers
│   │           ├── __init__.py
│   │           ├── anchor_generator.py    ## Anchor generator
│   │           ├── backbone
│   │           │   ├── __init__.py
│   │           │   ├── resnet.py          ## ResNet
│   │           │   └── resnext.py         ## ResNeXt
│   │           ├── base_dense_head.py     ## Base class for DenseHeads
│   │           ├── bbox_coder.py          ## Implemented BoundingBbox coder
│   │           ├── bbox_overlaps.py       ## Calculate bounding box overlaps
│   │           ├── conv_module.py         ## Convolution module
│   │           ├── focal_loss.py          ## Focal loss (part of loss function for training RetinaNet model)
│   │           ├── fpn.py                 ## Feature Pyramid Network
│   │           ├── initialization
│   │           │   ├── __init__.py
│   │           │   └── initialization.py  ## Initialization of layer weights (parameters)
│   │           ├── init_params.py         ## Parameters utils
│   │           ├── multi_box.py           ## RetinaNet dense bounding box head (regression, classification)
│   │           ├── nms.py                 ## Non-Maximum Suppression
│   │           └── retinanet.py           ## Provide retinanet training loss through network.
│   └── tools
│       ├── __init__.py
│       ├── callbacks.py                   ## Callbacks
│       ├── cell.py                        ## Functions of cells
│       ├── criterion.py                   ## Training criterion (target function)
│       ├── lr_schedule.py                 ## LR generator
│       ├── misc.py                        ## Misc functions for program
│       └── trainer.py                     ## Wrapper for training a model
└── train.py                               ## Train the model
```

### [Script Parameters](#contents)

Major parameters in the yaml config file as follows:

```shell
enable_modelarts: 0
data_url: ""
train_url: "/cache/data/freeanchor_models"
checkpoint_url: ""
data_path: "/cache/data"
output_path: "/cache/train"
load_path: "/cache/checkpoint_path"
enable_profiling: 0

train_outputs: '/data/freeanchor_models'
brief: 'gpu-1_1024x1024'                  ## Experiment short name
device_target: GPU                        ## Device
pynative_mode: 0                          ## Run in PYNATIVE mode (1) or in GRAPH mode (0)
# ==============================================================================

# model structure
backbone:
  type: 'ResNet'             ## Backbone type (resnet or resnext)
  depth: 50                  ## Backbone depth
  num_stages: 4              ## Number of resnet/resnext stages
  out_indices: [0, 1, 2, 3]  ## Define indices of resnet/resnext outputs
  frozen_stages: 1           ## Number of frozen stages (use if backbone is pretrained, set -1 to not freeze)
  norm_type: 'BN'
  norm_eval: True            ## Whether make batch normalization work in eval mode all time (use if backbone is pretrained)

# neck
neck:
  fpn:
    in_channels: [256, 512, 1024, 2048]  ## Channels number for each input feature map (FPN)
    out_channels: 256                    ## Channels number for each output feature map (FPN)
    start_level: 1
    num_outs: 5                          ## Number of output feature map (FPN)
    add_extra_convs: 'on_input'

# bbox head
bbox_head:
  type: 'RetinaHead'
  num_classes: 80
  in_channels: 256                      ## Number of input channels
  stacked_convs: 4
  feat_channels: 256                    ## Number of intermediate channels before classification
  anchor_generator:
    type: 'AnchorGenerator'
    octave_base_scale: 4                ## Anchor scales
    scales_per_octave: 3                ## Anchor scales
    ratios: [0.5, 1.0, 2.0]             ## Anchor ratios
    strides: [8, 16, 32, 64, 128]       ## Anchor strides for each feature map
  bbox_coder:
    type: 'DeltaXYWHBBoxCoder'
    target_means: [0.0, 0.0, 0.0, 0.0]  ## Bounding box coder parameter
    target_stds: [0.1, 0.1, 0.2, 0.2]   ## Bounding box coder parameter
  loss_bbox:
    type: 'SmoothL1Loss'
    beta: 0.11
    loss_weight: 0.75                   ## Localization loss weight
  assigner:
    pos_iou_thr: 0.5                    ## IoU threshold for positive bboxes
    neg_iou_thr: 0.4                    ## IoU threshold for negative bboxes
    min_pos_iou: 0                      ## Minimum IOU for a bbox to be considered as a positive bbox
    ignore_iof_thr: -1
    allowed_border: -1
    pos_weight: -1
bbox_loss_cls_use_sigmoid: True

feature_size: [75, 38, 19, 10, 5]
extras_out_channels: [256, 256, 256, 256, 256]
num_default: [9, 9, 9, 9, 9]
num_retinanet_boxes: 67995  # 66870  # 67995
prior_scaling: [0.1, 0.2]
gamma: 2.0
alpha: 0.75

# training loss
loss_beta: 0.11
loss_weight: 0.75

# training parameters
optim_grad_max_norm: 35
optim_norm_type: 2
optimizer: 'SGD'         ## Optimizer type (sgd or adam)
lr: 0.0001               ## Base learning rate
loss_scale: 1
momentum: 0.9            ## Optimizer parameter
weight_decay: 0.0001     ## Regularization
amp_level: 'O0'

# learning policy
lr_policy: 'step'
lr_warmup: 'linear'
lr_warmup_iters: 500     ## Number of warmup steps
lr_warmup_ratio: 0.0011  ## Initial learning rate = base_lr * warmup_ratio
lr_step: [8, 11]         ## Epochs numbers when learning rate is divided by 10 (for multistep lr_type)
epochs: 10               ## Number of epochs

# dataset
train_dataset: '/datasets/coco-2017/train'                     ## Path to train dataset
train_dataset_ann: '/datasets/coco-2017/train/labels.json'     ## Path to train dataset annotation
val_dataset: '/datasets/coco-2017/validation/'                 ## Path to validation dataset
val_dataset_ann: '/datasets/coco-2017/validation/labels.json'  ## Path to validation dataset annotation
train_data_type: 'coco'                                        ## Train dataset type
val_data_type: 'mindrecord'                                    ## Validation dataset type
train_dataset_num: -1                                          ## Parameter to reduce training data (debugging)
test_dataset_num: 10                                           ## Parameter to reduce test data (debugging)
train_dataset_divider: null                                    ## Parameter to reduce training data (debugging)
batch_size: 1                                                  ## Train batch size
num_parallel_workers: 1                                        ## Number of threads used to process the dataset in parallel
num_workers: 1
python_multiprocessing: False                                  ## Parallelize Python operations with multiple worker processes
eval_interval: 1
eval_metric: 'bbox'
to_rgb: True                                                   ## RGB or BGR
keep_ratio: True                                               ## Keep ratio in original images
flip_ratio: 0.5                                                ## Probability of image horizontal flip
expand_ratio: 0.0                                              ## Probability of image expansion

divider: 64                                                    ## Automatically make width and height are dividable by divider
img_width: 1024                                                ## Input images width
img_height: 1024                                               ## Input images height
img_mean: [123.675, 116.28, 103.53]                            ## Image normalization parameters
img_std: [58.395, 57.12, 57.375]                               ## Image normalization parameters
test_batch_size: 1                                             ## Test batch size


num_gts: 67995

# train configuration
assigner_type: 'MaxIoUAssigner'
assigner_pos_iou_thr: 0.5
assigner_neg_iou_thr: 0.4
assigner_min_pos_iou: 0
assigner_ignore_iof_thr: -1
allowed_border: -1
pos_weight: -1

# test configuration
test_cfg:
  nms_pre: 1000
  min_bbox_size: 0
  score_thr: 0.05                                              ## Confidence threshold
  max_per_img: 100
  nms:
    type: 'nms'
    iou_threshold: 0.5                                         ## IOU threshold

# runtime
device_num: 1                                                  ## Number of devices (only if distributed training turned on)
rank_id: 0

coco_classes: ["background", "person", "bicycle", "car", "motorcycle", "airplane", "bus",
               "train", "truck", "boat", "traffic light", "fire hydrant",
               "stop sign", "parking meter", "bench", "bird", "cat", "dog",
               "horse", "sheep", "cow", "elephant", "bear", "zebra",
               "giraffe", "backpack", "umbrella", "handbag", "tie",
               "suitcase", "frisbee", "skis", "snowboard", "sports ball",
               "kite", "baseball bat", "baseball glove", "skateboard",
               "surfboard", "tennis racket", "bottle", "wine glass", "cup",
               "fork", "knife", "spoon", "bowl", "banana", "apple",
               "sandwich", "orange", "broccoli", "carrot", "hot dog", "pizza",
               "donut", "cake", "chair", "couch", "potted plant", "bed",
               "dining table", "toilet", "tv", "laptop", "mouse", "remote",
               "keyboard", "cell phone", "microwave", "oven", "toaster", "sink",
               "refrigerator", "book", "clock", "vase", "scissors",
               "teddy bear", "hair drier", "toothbrush"]

src: 'retinanet_r50.ckpt'
export_format: 'MINDIR'
 ```

## [Training](#contents)

To train the model, run `train.py`.

### [Training process](#contents)

Standalone training mode:

```bash
bash scripts/run_standalone_train_gpu.sh [CONFIG_PATH] [TRAIN_DATA] [VAL_DATA] [TRAIN_OUT] [BRIEF] (OPTIONAL)[PRETRAINED_PATH]
```

We need several parameters for these scripts.

* `CONFIG_PATH`: path to config file.
* `TRAIN_DATA`: path to train dataset.
* `VAL_DATA`: path to validation dataset.
* `TRAIN_OUT`: path to folder with training experiments.
* `BRIEF`: short experiment name.
* `PRETRAINED_PATH`: the path of pretrained checkpoint file, it is better
 to use absolute path.

Training result will be stored in the current path, whose folder name is "LOG".
Under this, you can find checkpoint files together with result like the
following in log.

## [Evaluation](#contents)

### [Evaluation process](#contents)

#### [Evaluation on GPU](#contents)

```shell
bash scripts/run_eval_gpu.sh [CONFIG_PATH] [VAL_DATA] [CHECKPOINT_PATH] (Optional)[PREDICTION_PATH]
```

We need four parameters for this scripts.

* `CONFIG_PATH`: path to config file.
* `VAL_DATA`: the absolute path for dataset subset (validation).
* `CHECKPOINT_PATH`: path to checkpoint.
* `PREDICTION_PATH`: path to file with predictions JSON file (predictions may
 be saved to this file and loaded after).

> checkpoint can be produced in training process.

### [Evaluation result](#contents)

Result for GPU:

```log
CHECKING MINDRECORD FILES DONE!
Start Eval!
loading annotations into memory...
Done (t=0.48s)
creating index...
index created!

========================================

total images num:  5000
Processing, please wait a moment.
100%|██████████| 5000/5000 [1:02:34<00:00,  1.33it/s]
Loading and preparing results...
Converting ndarray to lists...
(138134, 7)
0/138134
DONE (t=0.92s)
creating index...
index created!
Running per image evaluation...
Evaluate annotation type *bbox*
DONE (t=27.15s).
Accumulating evaluation results...
DONE (t=4.65s).
 Average Precision  (AP) @[ IoU=0.50:0.95 | area=   all | maxDets=100 ] = 0.376
 Average Precision  (AP) @[ IoU=0.50      | area=   all | maxDets=100 ] = 0.567
 Average Precision  (AP) @[ IoU=0.75      | area=   all | maxDets=100 ] = 0.402
 Average Precision  (AP) @[ IoU=0.50:0.95 | area= small | maxDets=100 ] = 0.186
 Average Precision  (AP) @[ IoU=0.50:0.95 | area=medium | maxDets=100 ] = 0.415
 Average Precision  (AP) @[ IoU=0.50:0.95 | area= large | maxDets=100 ] = 0.514
 Average Recall     (AR) @[ IoU=0.50:0.95 | area=   all | maxDets=  1 ] = 0.320
 Average Recall     (AR) @[ IoU=0.50:0.95 | area=   all | maxDets= 10 ] = 0.512
 Average Recall     (AR) @[ IoU=0.50:0.95 | area=   all | maxDets=100 ] = 0.544
 Average Recall     (AR) @[ IoU=0.50:0.95 | area= small | maxDets=100 ] = 0.325
 Average Recall     (AR) @[ IoU=0.50:0.95 | area=medium | maxDets=100 ] = 0.592
 Average Recall     (AR) @[ IoU=0.50:0.95 | area= large | maxDets=100 ] = 0.713
Eval result: 0.37591040122090236

Evaluation done!

Done!
Time taken: 3797 seconds
```

## [Metrics](#metrics)

### `retinanet_free_anchor_r50_fpn_1x_coco`

```shell
Average Precision  (AP) @[ IoU=0.50:0.95 | area=   all | maxDets=100 ] = 0.376
Average Precision  (AP) @[ IoU=0.50      | area=   all | maxDets=100 ] = 0.567
Average Precision  (AP) @[ IoU=0.75      | area=   all | maxDets=100 ] = 0.402
Average Precision  (AP) @[ IoU=0.50:0.95 | area= small | maxDets=100 ] = 0.186
Average Precision  (AP) @[ IoU=0.50:0.95 | area=medium | maxDets=100 ] = 0.415
Average Precision  (AP) @[ IoU=0.50:0.95 | area= large | maxDets=100 ] = 0.514
Average Recall     (AR) @[ IoU=0.50:0.95 | area=   all | maxDets=  1 ] = 0.320
Average Recall     (AR) @[ IoU=0.50:0.95 | area=   all | maxDets= 10 ] = 0.512
Average Recall     (AR) @[ IoU=0.50:0.95 | area=   all | maxDets=100 ] = 0.544
Average Recall     (AR) @[ IoU=0.50:0.95 | area= small | maxDets=100 ] = 0.325
Average Recall     (AR) @[ IoU=0.50:0.95 | area=medium | maxDets=100 ] = 0.592
Average Recall     (AR) @[ IoU=0.50:0.95 | area= large | maxDets=100 ] = 0.713
```

* Eval result: 0.37591040122090236
* Need:        0.3815
* Diff:        0.0056 (0.56%)

### `retinanet_free_anchor_r101_fpn_1x_coco`

```shell
Average Precision  (AP) @[ IoU=0.50:0.95 | area=   all | maxDets=100 ] = 0.391
Average Precision  (AP) @[ IoU=0.50      | area=   all | maxDets=100 ] = 0.583
Average Precision  (AP) @[ IoU=0.75      | area=   all | maxDets=100 ] = 0.418
Average Precision  (AP) @[ IoU=0.50:0.95 | area= small | maxDets=100 ] = 0.203
Average Precision  (AP) @[ IoU=0.50:0.95 | area=medium | maxDets=100 ] = 0.434
Average Precision  (AP) @[ IoU=0.50:0.95 | area= large | maxDets=100 ] = 0.537
Average Recall     (AR) @[ IoU=0.50:0.95 | area=   all | maxDets=  1 ] = 0.331
Average Recall     (AR) @[ IoU=0.50:0.95 | area=   all | maxDets= 10 ] = 0.525
Average Recall     (AR) @[ IoU=0.50:0.95 | area=   all | maxDets=100 ] = 0.558
Average Recall     (AR) @[ IoU=0.50:0.95 | area= small | maxDets=100 ] = 0.328
Average Recall     (AR) @[ IoU=0.50:0.95 | area=medium | maxDets=100 ] = 0.608
Average Recall     (AR) @[ IoU=0.50:0.95 | area= large | maxDets=100 ] = 0.739
```

* Eval result: 0.39088849847334417
* Need:        0.3968
* Diff:        0.006 (0.6%)

### `retinanet_free_anchor_x101_32x4d_fpn_1x_coco`

```shell
Average Precision  (AP) @[ IoU=0.50:0.95 | area=   all | maxDets=100 ] = 0.410
Average Precision  (AP) @[ IoU=0.50      | area=   all | maxDets=100 ] = 0.605
Average Precision  (AP) @[ IoU=0.75      | area=   all | maxDets=100 ] = 0.442
Average Precision  (AP) @[ IoU=0.50:0.95 | area= small | maxDets=100 ] = 0.215
Average Precision  (AP) @[ IoU=0.50:0.95 | area=medium | maxDets=100 ] = 0.458
Average Precision  (AP) @[ IoU=0.50:0.95 | area= large | maxDets=100 ] = 0.555
Average Recall     (AR) @[ IoU=0.50:0.95 | area=   all | maxDets=  1 ] = 0.340
Average Recall     (AR) @[ IoU=0.50:0.95 | area=   all | maxDets= 10 ] = 0.540
Average Recall     (AR) @[ IoU=0.50:0.95 | area=   all | maxDets=100 ] = 0.573
Average Recall     (AR) @[ IoU=0.50:0.95 | area= small | maxDets=100 ] = 0.343
Average Recall     (AR) @[ IoU=0.50:0.95 | area=medium | maxDets=100 ] = 0.624
Average Recall     (AR) @[ IoU=0.50:0.95 | area= large | maxDets=100 ] = 0.749
```

* Eval result: 0.409960804304291
* Need:        0.4154
* Diff:        0.0055 (0.55%)

## [Model Description](#contents)

### [Performance](#contents)

| Parameters          | GPU                                      |
| ------------------- |------------------------------------------|
| Model Version       | FreeAnchor ResNet50                      |
| Resource            | NVIDIA GeForce RTX 3090 (x4)             |
| Uploaded Date       | 08/29/2023 (month/day/year)              |
| MindSpore Version   | 1.10.0                                   |
| Dataset             | COCO2017                                 |
| Pretrained          | ???                                      |
| Training Parameters | epoch = 12,  batch_size = 4 (per device) |
| Optimizer           | SGD (momentum)                           |
| Loss Function       | SmoothL1Loss                             |
| Speed               | ???                                      |
| Total time          | ???                                      |
| outputs             | mAP                                      |
| mAP                 | ???                                      |
| Model for inference | ???                                      |
| configuration       | resnet50_train_config.yaml               |
| Scripts             |                                          |

## [Description of Random Situation](#contents)

We use random seed in train.py.

## [ModelZoo Homepage](#contents)

Please check the official [homepage](https://gitee.com/mindspore/models).

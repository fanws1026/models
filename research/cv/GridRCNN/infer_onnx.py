# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
"""Infer ONNX model."""
import argparse
import os
import json
from pprint import pprint

from pathlib import Path
import numpy as np
import onnxruntime as ort
from tqdm import tqdm
from src.model_utils.config import (
    parse_yaml, parse_cli_to_yaml, merge, Config, compute_features_info
)
from src.dataset import (
    imnormalize_column, rescale_column_test, resize_column_test
)
from infer import imread


def get_config():
    """
    Get Config according to the yaml file and cli arguments.
    """
    parser = argparse.ArgumentParser(description='default name',
                                     add_help=False)
    current_dir = os.path.dirname(os.path.abspath(__file__))
    parser.add_argument('--config_path', type=str,
                        default=os.path.join(current_dir, 'configs',
                                             'default_config.yaml'),
                        help='Config file path')
    parser.add_argument('--onnx_path', help='Path to save checkpoint.')
    parser.add_argument('--pred_output', help='Path to model predictions.')
    parser.add_argument('--pred_input', help='Path to model predictions.')
    path_args, _ = parser.parse_known_args()
    default, helper, choices = parse_yaml(path_args.config_path)
    args = parse_cli_to_yaml(parser=parser, cfg=default, helper=helper,
                             choices=choices, cfg_path=path_args.config_path)
    final_config = Config(merge(args, default))
    final_config = compute_features_info(final_config)
    pprint(final_config)
    print('Please check the above information for the configurations',
          flush=True)
    return final_config


def data_loader(path: Path, config):
    """Load image or images from folder in generator."""

    def process_img(img):
        data = (
            img, img.shape[:2], np.zeros((0, 4)), np.zeros((0, 1)),
            np.zeros((0, 1))
        )
        if config.keep_ratio:
            data = rescale_column_test(*data, config=config)
        else:
            data = resize_column_test(*data, config=config)
        img, meta, *_ = imnormalize_column(*data, config=config)
        img = img.transpose(2, 0, 1).copy()
        img = img.astype(np.float32)
        return img, meta

    extensions = ('.png', '.jpg', '.jpeg')
    path = path.resolve()
    if path.is_dir():
        for item in path.iterdir():
            if item.is_dir():
                continue
            if item.suffix.lower() not in extensions:
                continue
            image = imread(str(item))
            image = image[..., ::-1].copy()
            tensor, img_meta = process_img(image)

            yield str(item), img_meta[np.newaxis, :], tensor[np.newaxis, :]
    else:
        image = imread(str(path))
        image = image[..., ::-1].copy()
        tensor, img_meta = process_img(image)

        yield str(path), img_meta[np.newaxis, :], tensor[np.newaxis, :]


def create_session(checkpoint_path, target_device):
    """Create ONNX runtime session"""
    if target_device == 'GPU':
        providers = ['CUDAExecutionProvider']
    elif target_device in ('CPU', 'Ascend'):
        providers = ['CPUExecutionProvider']
    else:
        raise ValueError(
            f"Unsupported target device '{target_device}'. "
            f"Expected one of: 'CPU', 'GPU', 'Ascend'"
        )
    session = ort.InferenceSession(checkpoint_path, providers=providers)
    input_names = [x.name for x in session.get_inputs()]
    return session, input_names


def main():
    config = get_config()
    session, input_names = create_session(
        config.onnx_path, config.device_target
    )

    data_generator = data_loader(Path(config.pred_input), config)

    predictions = {}
    for name, meta, tensor in tqdm(data_generator):
        input_data = [tensor, meta]
        outputs = session.run(None, dict(zip(input_names, input_data)))
        bboxes, labels, masks = outputs

        bboxes_squee = np.squeeze(bboxes[0, :, :])
        labels_squee = np.squeeze(labels[0, :, :])
        masks_squee = np.squeeze(masks[0, :, :])

        meta = meta[0]

        bboxes_mask = bboxes_squee[masks_squee, :4]
        bboxes_mask[::, [2, 3]] -= bboxes_mask[::, [0, 1]] - 1

        confs_mask = bboxes_squee[masks_squee, 4]
        labels_mask = labels_squee[masks_squee]

        predictions[name] = {
            'height': int(meta[0]),
            'width': int(meta[1]),
            'predictions': []
        }
        for box, conf, label in zip(bboxes_mask, confs_mask, labels_mask):
            predictions[name]['predictions'].append({
                'bbox': {
                    'x_min': float(box[0]),
                    'y_min': float(box[1]),
                    'width': float(box[2]),
                    'height': float(box[3])
                },
                'class': {
                    'label': int(label),
                    'category_id': 'unknown',
                    'name': config.coco_classes[int(label) + 1]
                },
                'score': float(conf)
            })

    with open(config.pred_output, 'w', encoding='utf-8') as f:
        json.dump(predictions, f, indent=1)


if __name__ == '__main__':
    main()

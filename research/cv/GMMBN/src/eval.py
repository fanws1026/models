import os
import random
import time
import mindspore
import mindspore.nn as nn

import models
# from tensorboardX import SummaryWriter
from utils.utils import get_args
from dataset.dataloader import data_loader
from loss import get_loss

best_acc1 = 0

def main():
    args = get_args()
    random.seed(args.data_seed)
    mindspore.set_seed(args.data_seed)
    main_worker(args)

def main_worker(args):
    # create model
    print("=> creating model '{}'".format(args.arch))
    net = models.create_model(args)

    base_lr = 0.1*args.batch_size/256 if args.lsr else args.base_lr ##Linear Learning rate scaling

    params = net.get_parameters() #models.split_weights(model)

    optimizer = nn.SGD(params, base_lr, momentum=args.momentum, weight_decay=args.weight_decay)

    # Data loading code
    start = time.time()
    args, train_dataset_weak, _, val_dataset = data_loader(args)
    _ = train_dataset_weak.get_dataset_size()
    print("The dataset size is:", train_dataset_weak.get_dataset_size())
    end = time.time()
    print("data_loader time:", end-start)

    criterion = get_loss(args)
    # criterion = nn.SoftmaxCrossEntropyWithLogits(sparse=True, reduction="mean")

    if os.path.exists(args.resume_checkpoint):
        print("=> loading checkpoint '{}'".format(args.resume_checkpoint))
        checkpoint = mindspore.load_checkpoint(args.resume_checkpoint)
        mindspore.load_param_into_net(net, checkpoint)
        print("=> loaded checkpoint '{}'".format(args.resume_checkpoint))
    model = mindspore.Model(network=net, loss_fn=criterion, optimizer=optimizer, metrics={"acc"})
    # model.train(1, train_dataset=train_dataset_weak, callbacks=[LossMonitor(0.005)])
    net.set_train(False)
    acc = model.eval(val_dataset)
    print("acc:", acc)



if __name__ == '__main__':
    mindspore.set_context(mode=mindspore.PYNATIVE_MODE, device_target='Ascend', device_id=0)
    main()

// Copyright 2023 Huawei Technologies Co., Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// ============================================================================
//
// This file or its part has been derived from the following repository
// and modified: https://github.com/open-mmlab/mmcv/tree/v1.7.1
// ============================================================================
#ifndef MASKED_CONV2D_CUDA_KERNEL_CUH
#define MASKED_CONV2D_CUDA_KERNEL_CUH

#include <float.h>
#include <cuda.h>
#include <stdio.h>
#include "./custom_aot_extra.h"


#define THREADS_PER_BLOCK 512


inline int GET_BLOCKS(const int N, const int num_threads = THREADS_PER_BLOCK) {
  int optimal_block_num = (N + num_threads - 1) / num_threads;
  int max_block_num = 4096;
  return min(optimal_block_num, max_block_num);
}

#define CUDA_1D_KERNEL_LOOP(i, n)                              \
  for (int i = blockIdx.x * blockDim.x + threadIdx.x; i < (n); \
       i += blockDim.x * gridDim.x)

template <typename T>
__global__ void fill_zeros_cuda_kernel(const int nthreads, T* tensor) {
    CUDA_1D_KERNEL_LOOP(index, nthreads) {
        tensor[index] = 0.0;
    }
}


template <typename scalar_t>
__global__ void MaskedIm2colForward(const int n, const scalar_t *data_im,
                                    const int height, const int width,
                                    const int* kernel_h_, const int* kernel_w_,
                                    const int* pad_h_, const int* pad_w_,
                                    const int64_t *mask_h_idx,
                                    const int64_t *mask_w_idx,
                                    const int mask_cnt, scalar_t *data_col) {
  int kernel_h = *kernel_h_;
  int kernel_w = *kernel_w_;
  int pad_h = *pad_h_;
  int pad_w = *pad_w_;

  // mask_cnt * channels
  CUDA_1D_KERNEL_LOOP(index, n) {
    const int m_index = index % mask_cnt;
    const int h_col = mask_h_idx[m_index];
    const int w_col = mask_w_idx[m_index];

    // check, that index > 0 or we need to skip any computatations
    if (h_col >= 0 || w_col >=0) {
        const int c_im = index / mask_cnt;
        const int c_col = c_im * kernel_h * kernel_w;
        const int h_offset = h_col - pad_h;
        const int w_offset = w_col - pad_w;
        scalar_t *data_col_ptr = data_col + c_col * mask_cnt + m_index;

        for (int i = 0; i < kernel_h; ++i) {
          int h_im = h_offset + i;
          for (int j = 0; j < kernel_w; ++j) {
            int w_im = w_offset + j;
            if (h_im >= 0 && w_im >= 0 && h_im < height && w_im < width) {
              *data_col_ptr =
                  (scalar_t)data_im[(c_im * height + h_im) * width + w_im];
            } else {
              *data_col_ptr = 0.0;
            }
            data_col_ptr += mask_cnt;
          }
        }
      }
    }
}


template <typename scalar_t>
__global__ void MaskedCol2imForward(const int n, const scalar_t *data_col,
                                    const int height, const int width,
                                    const int channels,
                                    const int64_t *mask_h_idx,
                                    const int64_t *mask_w_idx,
                                    const int mask_cnt, scalar_t *data_im) {
  CUDA_1D_KERNEL_LOOP(index, n) {
    const int m_index = index % mask_cnt;
    const int h_im = mask_h_idx[m_index];
    const int w_im = mask_w_idx[m_index];
    const int c_im = index / mask_cnt;
    if (h_im >= 0 || w_im >=0) {
    // compute the start and end of the output
        data_im[(c_im * height + h_im) * width + w_im] = data_col[index];
    }
  }
}


extern "C" int CustomMaskedIm2colForward(int nparam, void **params, int *ndims, int64_t **shapes,
                                         const char **dtypes, void *stream, void *extra_void) {
    cudaStream_t custream = static_cast<cudaStream_t>(stream);

    const int channels = static_cast<int>(shapes[0][1]);
    const int height = static_cast<int>(shapes[0][2]);
    const int width = static_cast<int>(shapes[0][3]);

    const int mask_cnt = static_cast<int>(shapes[1][0]);

    // kernel width (tensor with 1 value. in the python code: ms.Tensor(kernel_w))
    int * kernel_w = static_cast<int *>(params[3]);
    // kernel height (tensor with 1 value. in the python code: ms.Tensor(kernel_h))
    int * kernel_h = static_cast<int *>(params[4]);

    // padding (tensor with 1 value. in the python code: ms.Tensor(padding))
    int * pad_h = static_cast<int *>(params[5]);
    int * pad_w = static_cast<int *>(params[5]);



    const float * data_im = static_cast<float *>(params[0]);
    const int64_t * mask_h_idx = static_cast<int64_t *>(params[1]);
    const int64_t * mask_w_idx = static_cast<int64_t *>(params[2]);

    // output tensor
    float *data_col = static_cast<float *>(params[6]);

    const int output_size = mask_cnt * channels;

    fill_zeros_cuda_kernel<<<GET_BLOCKS(output_size), THREADS_PER_BLOCK, 0, custream>>> (
        output_size,
        data_col);

    MaskedIm2colForward<<<GET_BLOCKS(output_size), THREADS_PER_BLOCK, 0, custream>>>(
        output_size,
        data_im,
        height,
        width,
        kernel_h,
        kernel_w,
        pad_h,
        pad_w,
        mask_h_idx,
        mask_w_idx,
        mask_cnt,
        data_col);
    return 0;
}


extern "C" int CustomMaskedCol2imForward(int nparam, void **params, int *ndims, int64_t **shapes,
                                         const char **dtypes, void *stream, void *extra_void) {
    cudaStream_t custream = static_cast<cudaStream_t>(stream);

    const int channels = static_cast<int>(shapes[4][1]);
    const int height = static_cast<int>(shapes[4][2]);
    const int width = static_cast<int>(shapes[4][3]);
    const int mask_cnt = static_cast<int>(shapes[2][0]);

    const float * features = static_cast<float *>(params[0]);
    const int64_t * mask_h_idx = static_cast<int64_t *>(params[1]);
    const int64_t * mask_w_idx = static_cast<int64_t *>(params[2]);
    const float *masked_output = static_cast<float *>(params[3]);

    float * output = static_cast<float *>(params[4]);

    const int output_size = mask_cnt * channels;


    fill_zeros_cuda_kernel<<<GET_BLOCKS(output_size), THREADS_PER_BLOCK, 0, custream>>> (
        output_size,
        output);

    MaskedCol2imForward<<<GET_BLOCKS(output_size), THREADS_PER_BLOCK, 0, custream>>>(
        output_size,
        masked_output,
        height,
        width,
        channels,
        mask_h_idx,
        mask_w_idx,
        mask_cnt,
        output);
    return 0;
}

#endif  // MASKED_CONV2D_CUDA_KERNEL_CUH

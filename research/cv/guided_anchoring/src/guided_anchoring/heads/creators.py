# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
from typing import Optional, Union

from ...config import Config
from .ga_rpn_head import GARPNHead
from .ga_retina_head import GARetinaHead
from ..anchor import create_anchor_generator
from ..bbox import create_coder, create_sampler, create_assigner
from ..losses import (create_loss_cls, create_loss_bbox, create_loss_shape,
                      create_loss_loc)


def create_head(
        config: Config,
        test_cfg: Config,
        train_cfg: Optional[Config] = None,
) -> Union[GARPNHead, GARetinaHead]:
    approx_anchor_generator = create_anchor_generator(
        config.approx_anchor_generator)
    square_anchor_generator = create_anchor_generator(
        config.square_anchor_generator)
    anchor_coder = create_coder(config.anchor_coder)
    bbox_coder = create_coder(config.bbox_coder)

    loss_bbox = create_loss_bbox(config.loss_bbox)
    loss_cls = create_loss_cls(config.loss_cls)
    loss_loc = create_loss_loc(config.loss_loc)
    loss_shape = create_loss_shape(config.loss_shape)

    assigner = None
    ga_assigner = None
    ga_sampler = None
    sampler = None

    if config.type == 'GARPNHead':
        if train_cfg:
            assigner = create_assigner(train_cfg.rpn.assigner)
            ga_assigner = create_assigner(train_cfg.rpn.ga_assigner)
            sampler = create_sampler(train_cfg.rpn.get('sampler'))
            ga_sampler = create_sampler(train_cfg.rpn.get('ga_sampler'))

        return GARPNHead(
            in_channels=config.in_channels,
            approx_anchor_generator=approx_anchor_generator,
            square_anchor_generator=square_anchor_generator,
            anchor_coder=anchor_coder,
            bbox_coder=bbox_coder,
            loss_loc=loss_loc,
            loss_bbox=loss_bbox,
            loss_cls=loss_cls,
            loss_shape=loss_shape,
            loc_filter_thr=config.loc_filter_thr,
            feat_channels=config.feat_channels,
            use_sigmoid_cls=config.loss_cls.get('use_sigmoid', False),
            loc_focal_loss=config.loss_loc.type in ['FocalLoss'],
            sampling=config.loss_loc.type not in ['FocalLoss'],
            test_cfg=test_cfg.rpn.cfg_dict,
            train_cfg=train_cfg.rpn.cfg_dict if train_cfg else None,
            assigner=assigner,
            ga_assigner=ga_assigner,
            sampler=sampler,
            ga_sampler=ga_sampler
        )
    if config.type == 'GARetinaHead':
        if train_cfg:
            assigner = create_assigner(train_cfg.assigner)
            ga_assigner = create_assigner(train_cfg.ga_assigner)
            sampler = create_sampler(train_cfg.get('sampler'))
            ga_sampler = create_sampler(train_cfg.get('ga_sampler'))

        return GARetinaHead(
            num_classes=config.num_classes,
            in_channels=config.in_channels,
            stacked_convs=config.stacked_convs,
            approx_anchor_generator=approx_anchor_generator,
            square_anchor_generator=square_anchor_generator,
            anchor_coder=anchor_coder,
            bbox_coder=bbox_coder,
            loss_loc=loss_loc,
            loss_bbox=loss_bbox,
            loss_cls=loss_cls,
            loss_shape=loss_shape,
            use_sigmoid_cls=config.loss_cls.get('use_sigmoid', False),
            loc_focal_loss=config.loss_loc.type in ['FocalLoss'],
            sampling=config.loss_loc.type not in ['FocalLoss'],
            test_cfg=test_cfg.cfg_dict,
            train_cfg=train_cfg.cfg_dict if train_cfg else None,
            feat_channels=config.feat_channels,
            ga_assigner=ga_assigner,
            assigner=assigner,
            sampler=sampler,
            ga_sampler=ga_sampler,
            ga_sampling=ga_sampler is not None,
        )
    raise RuntimeError(f'Unknown head type: {config.type}')

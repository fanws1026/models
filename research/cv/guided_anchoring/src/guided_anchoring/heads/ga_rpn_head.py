# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
#
# This file has been derived from the
# https://github.com/open-mmlab/mmdetection/tree/v2.28.2
# repository and modified.
# ============================================================================
import mindspore as ms
import mindspore.nn as nn
import mindspore.ops as ops

from .guided_anchor_head import GuidedAnchorHead


class GARPNHead(GuidedAnchorHead):
    """Guided-Anchor-based RPN head."""

    def __init__(self,
                 in_channels,
                 **kwargs):
        super().__init__(num_classes=1, in_channels=in_channels, **kwargs)

        self.relu = nn.ReLU()
        self.nms = ops.NMSWithMask(0.7)
        self.concat_axis1 = ops.Concat(axis=1)
        self.min_float_num = -45500.0

    def get_bboxes(self,
                   cls_scores,
                   bbox_preds,
                   shape_preds,
                   loc_preds,
                   img_metas,
                   cfg=None,
                   rescale=False):

        featmap_sizes = [featmap.shape[-2:] for featmap in cls_scores]
        # get guided anchors
        _, guided_anchors, loc_masks = self.get_anchors(
            featmap_sizes,
            shape_preds,
            loc_preds,
            img_metas,
            use_loc_filter=not self.training)

        result_list = []

        for img_id in range(len(img_metas)):
            cls_score_list = [
                cls_scores[i][img_id] for i in range(self.num_layers)
            ]
            bbox_pred_list = [
                bbox_preds[i][img_id] for i in range(self.num_layers)
            ]
            guided_anchor_list = [
                guided_anchors[img_id][i] for i in range(self.num_layers)
            ]
            loc_mask_list = [
                loc_masks[img_id][i] for i in range(self.num_layers)
            ]
            img_shape = img_metas[img_id]

            proposals = self._get_bboxes_single(cls_score_list, bbox_pred_list,
                                                guided_anchor_list,
                                                loc_mask_list, img_shape,
                                                1.0, cfg, rescale)

            result_list.append(proposals)
        return result_list

    def simple_test_rpn(self, x, img_metas):
        """
        Test without augmentation, only for ``RPNHead`` and its variants,
        e.g., ``GARPNHead``, etc.

        Args:
        ----
            x (tuple[Tensor]): Features from the upstream network, each is
                a 4D-tensor.
            img_metas: Meta info of each image.

        Returns:
        -------
            list[Tensor]: Proposals of each image, each item has shape (n, 5),
                where 5 represent (tl_x, tl_y, br_x, br_y, score).
        """
        cls_scores, bbox_preds, shape_preds, loc_preds = self([x])
        proposal_list = self.get_bboxes(
            cls_scores=cls_scores,
            bbox_preds=bbox_preds,
            shape_preds=shape_preds,
            loc_preds=loc_preds,
            img_metas=img_metas
        )

        return proposal_list

    def _init_layers(self):
        """Initialize layers of the head."""
        self.rpn_conv = nn.Conv2d(
            self.in_channels, self.feat_channels, 3,
            padding=1, pad_mode='pad', has_bias=True)
        super()._init_layers()

    def forward_single(self, x):
        """Forward feature of a single scale level."""
        x = self.rpn_conv(x)
        x = self.relu(x)
        (cls_score, bbox_pred, shape_pred,
         loc_pred) = super().forward_single(x)

        return cls_score, bbox_pred, shape_pred, loc_pred

    def loss(self,
             cls_scores,
             bbox_preds,
             shape_preds,
             loc_preds,
             gt_bboxes,
             img_metas,
             gt_bboxes_ignore=None):
        losses = super().loss(
            cls_scores,
            bbox_preds,
            shape_preds,
            loc_preds,
            gt_bboxes,
            None,
            img_metas,
            gt_bboxes_ignore=gt_bboxes_ignore)
        return dict(
            loss_rpn_cls=losses['loss_cls'],
            loss_rpn_bbox=losses['loss_bbox'],
            loss_anchor_shape=losses['loss_shape'],
            loss_anchor_loc=losses['loss_loc'])

    def loss_single(self, cls_score, bbox_pred, anchors, labels, label_weights,
                    bbox_targets, bbox_weights, num_total_samples):
        """
        Compute loss of a single scale level.

        Args:
        ----
            cls_score (Tensor): Box scores for each scale level
                Has shape (N, num_anchors * num_classes, H, W).
            bbox_pred (Tensor): Box energies / deltas for each scale
                level with shape (N, num_anchors * 4, H, W).
            anchors (Tensor): Box reference for each scale level with shape
                (N, num_total_anchors, 4).
            labels (Tensor): Labels of each anchors with shape
                (N, num_total_anchors).
            label_weights (Tensor): Label weights of each anchor with shape
                (N, num_total_anchors)
            bbox_targets (Tensor): BBox regression targets of each anchor
                weight shape (N, num_total_anchors, 4).
            bbox_weights (Tensor): BBox regression loss weights of each anchor
                with shape (N, num_total_anchors, 4).
            num_total_samples (int): If sampling, num total samples equal to
                the number of total anchors; Otherwise, it is the number of
                positive anchors.

        Returns:
        -------
            dict[str, Tensor]: A dictionary of loss components.
        """
        # classification loss
        labels = labels.flatten()
        labels = ops.expand_dims(labels, -1)
        labels = labels.astype(ms.float32)

        label_weights = label_weights.flatten()
        label_weights = label_weights.astype(ms.float32)

        cls_score_t = ops.transpose(cls_score, (0, 2, 3, 1))
        cls_score = ops.reshape(cls_score_t, (-1, self.cls_out_channels))
        # TODO: Add avg_factor
        loss_cls = self.loss_cls(cls_score, labels, label_weights)

        # regression loss
        bbox_targets = ops.Reshape()(bbox_targets, (-1, 4))
        bbox_weights = ops.Reshape()(bbox_weights, (-1, 4))
        bbox_pred = ops.Reshape()(
            ops.Transpose()(bbox_pred, (0, 2, 3, 1)),
            (-1, 4)
        )
        if self.reg_decoded_bbox:
            # When the regression loss (e.g. `IouLoss`, `GIouLoss`)
            # is applied directly on the decoded bounding boxes, it
            # decodes the already encoded coordinates to absolute format.
            anchors = ops.Reshape()(anchors, (-1, 4))
            bbox_pred = self.bbox_coder.decode(anchors, bbox_pred)
        loss_bbox = self.loss_bbox(bbox_pred, bbox_targets)
        loss_bbox = bbox_weights * loss_bbox
        loss_bbox = ops.reduce_sum(ops.reduce_mean(loss_bbox, -1), -1)
        return loss_cls, loss_bbox

    def _get_bboxes_single(self,
                           cls_scores,
                           bbox_preds,
                           mlvl_anchors,
                           mlvl_masks,
                           img_shape,
                           scale_factor,
                           cfg,
                           rescale=False):
        cfg = self.test_cfg if cfg is None else cfg

        mlvl_proposals = []
        mlvl_mask = []
        # same
        for idx in range(self.num_layers):
            rpn_cls_score = cls_scores[idx]
            rpn_bbox_pred = bbox_preds[idx]
            anchors = mlvl_anchors[idx]
            mask = mlvl_masks[idx]
            assert rpn_cls_score.shape[-2:] == rpn_bbox_pred.shape[-2:]
            rpn_cls_score = ops.transpose(rpn_cls_score, (1, 2, 0))

            if self.use_sigmoid_cls:
                rpn_cls_score = rpn_cls_score.reshape(-1)
                scores = ops.sigmoid(rpn_cls_score)
            else:
                rpn_cls_score = rpn_cls_score.reshape(-1, 2)
                # remind that we set FG labels to [0, num_class-1]
                # BG cat_id: num_class
                scores = rpn_cls_score.softmax(dim=1)[:, :-1]

            # filter scores w.r.t. mask.
            scores = ops.masked_fill(scores, ops.logical_not(mask),
                                     self.min_float_num)
            rpn_bbox_pred = ops.reshape(
                ops.transpose(rpn_bbox_pred, (1, 2, 0)),
                (-1, 4)
            )
            # same
            if self.nms_pre > 0 and scores.shape[0] > self.nms_pre:
                _, topk_inds = ops.topk(scores, self.nms_pre, sorted=True)
                topk_inds = ops.expand_dims(topk_inds, 1)
                rpn_bbox_pred = ops.gather_nd(rpn_bbox_pred, topk_inds)
                anchors = ops.gather_nd(anchors, topk_inds)
                scores = ops.gather_nd(scores, topk_inds)

            proposals = self.bbox_coder.decode(
                anchors, rpn_bbox_pred, max_shape=img_shape).reshape(-1, 4)
            proposals = ops.vstack((proposals.T, scores)).T
            proposals, _, mask_valid = self.nms(proposals)

            mlvl_proposals.append(proposals)
            mlvl_mask.append(mask_valid)

        mlvl_proposals = ops.concat(mlvl_proposals, 0)
        masks = ops.concat(mlvl_mask, 0)

        x1, y1, x2, y2, scores = (mlvl_proposals[:, 0], mlvl_proposals[:, 1],
                                  mlvl_proposals[:, 2], mlvl_proposals[:, 3],
                                  mlvl_proposals[:, 4])
        w = x2 - x1
        h = y2 - y1
        min_bbox_mask = ops.logical_and(
            ms.ops.ge(w, self.min_bbox_size),
            ms.ops.ge(h, self.min_bbox_size)
        ).reshape(-1)

        scores = ops.squeeze(scores)

        masks = ops.logical_and(masks, min_bbox_mask)
        masks = ops.logical_and(masks, scores > 0)

        scores = ops.masked_fill(scores, ops.logical_not(masks),
                                 self.min_float_num)
        _, topk_inds = ops.topk(scores, self.max_per_img, sorted=True)

        topk_inds = ops.expand_dims(topk_inds, 1)

        mlvl_proposals = ops.gather_nd(mlvl_proposals, topk_inds)
        masks = ops.ones(mlvl_proposals.shape[0], ms.bool_)

        return mlvl_proposals, masks

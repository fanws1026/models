# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
#
# This file has been derived from the
# https://github.com/open-mmlab/mmdetection/tree/v2.28.2
# repository and modified.
# ============================================================================
import mindspore as ms
import mindspore.numpy as np


def bbox2roi(bbox_list):
    """
    Convert a list of bboxes to roi format.

    Args:
    ----
        bbox_list (list[Tensor]): a list of bboxes corresponding to a batch
            of images.

    Returns:
    -------
        Tensor: shape (n, 5), [batch_ind, x1, y1, x2, y2]
    """
    rois_list = []
    for img_id in range(len(bbox_list)):
        bboxes = bbox_list[img_id]
        assert bboxes.shape[0] > 0
        img_inds = ms.ops.cast(np.full((bboxes.shape[0], 1), img_id),
                               ms.float32)
        rois = ms.ops.vstack([img_inds.T, bboxes.T[:4]]).T
        # else:
        #     rois = ms.ops.zeros((0, 5))
        rois_list.append(rois)
    rois = ms.ops.concat(rois_list, 0)
    return rois


def roi2bbox(rois):
    """
    Convert rois to bounding box format.

    Args:
    ----
        rois (Tensor): RoIs with the shape (n, 5) where the first
            column indicates batch id of each RoI.

    Returns:
    -------
        list[Tensor]: Converted boxes of corresponding rois.
    """
    bbox_list = []
    img_ids = ms.unique(rois[:, 0].cpu(), sorted=True)
    for img_id in img_ids:
        inds = (rois[:, 0] == img_id.item())
        bbox = rois[inds, 1:]
        bbox_list.append(bbox)
    return bbox_list


def bbox2result(bboxes, labels, num_classes):
    """
    Convert detection results to a list of numpy arrays.

    Args:
    ----
        bboxes (Tensor | np.ndarray): shape (n, 5)
        labels (Tensor | np.ndarray): shape (n, )
        num_classes (int): class number, including background class

    Returns:
    -------
        list(ndarray): bbox results of each class
    """
    if bboxes.shape[0] == 0:
        return [np.zeros((0, 5), dtype=np.float32) for i in range(num_classes)]
    if isinstance(bboxes, ms.Tensor):
        bboxes = bboxes.asnumpy()
        labels = labels.asnumpy()
    return [bboxes[labels == i, :] for i in range(num_classes)]

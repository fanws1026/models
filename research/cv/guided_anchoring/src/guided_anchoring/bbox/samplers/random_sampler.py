# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
import mindspore as ms
import mindspore.ops as ops

from .sampling_result import SamplingResult


class RandomSampler:
    """
    Random sampler.

    Args:
    ----
        num (int): Number of samples
        pos_fraction (float): Fraction of positive samples
        neg_pos_ub (int, optional): Upper bound number of negative and
            positive samples. Defaults to -1.
        add_gt_as_proposals (bool, optional): Whether to add ground truth
            boxes as proposals. Defaults to True.
    """

    def __init__(self,
                 num,
                 pos_fraction,
                 neg_pos_ub=-1,
                 add_gt_as_proposals=True,
                 **kwargs):
        self.num = num
        self.pos_fraction = pos_fraction
        self.neg_pos_ub = neg_pos_ub
        self.add_gt_as_proposals = add_gt_as_proposals

        self.num_positive = int(self.num * self.pos_fraction)
        self.num_negative = int(self.num * self.neg_pos_ub)

        device_target = ms.get_context('device_target')
        if device_target == 'GPU':
            self.randperm = ops.Randperm(10000, dtype=ms.int64)
        elif device_target == 'CPU':
            self.randperm = ops.randperm
        else:
            raise NotImplementedError('RandomSampler is not implemented '
                                      f'for device target {device_target}')

    def random_choice(self, gallery, num):
        """
        Random select some elements from the gallery.

        If `gallery` is a Tensor, the returned indices will be a Tensor;
        If `gallery` is a ndarray or list, the returned indices will be a
        ndarray.

        Args:
        ----
            gallery (Tensor | ndarray | list): indices pool.
            num (int): expected sample num.

        Returns:
        -------
            Tensor or ndarray: sampled indices.
        """
        assert len(gallery) >= num
        perm = self.randperm(ms.Tensor([gallery.size]))[:num]
        rand_inds = gallery[perm]
        return rand_inds

    def _sample_pos(self, assign_result, num_expected, **kwargs):
        """Randomly sample some positive samples."""
        pos_inds = (assign_result.gt_inds > 0).nonzero()
        if pos_inds.size != 0:
            pos_inds = ops.squeeze(pos_inds, 1)
        if pos_inds.size <= num_expected:
            return pos_inds
        return self.random_choice(pos_inds, num_expected)

    def _sample_neg(self, assign_result, num_expected, **kwargs):
        """Randomly sample some negative samples."""
        neg_inds = (assign_result.gt_inds == 0).nonzero()
        if neg_inds.size != 0:
            neg_inds = ops.squeeze(neg_inds, 1)
        if neg_inds.shape[0] <= num_expected:
            return neg_inds
        return self.random_choice(neg_inds, num_expected)

    def sample(self,
               assign_result,
               bboxes,
               gt_bboxes,
               gt_labels=None,
               **kwargs):
        """
        Sample positive and negative bboxes.

        This is a simple implementation of bbox sampling given candidates,
        assigning results and ground truth bboxes.

        Args:
        ----
            assign_result (:obj:`AssignResult`): Bbox assigning results.
            bboxes (Tensor): Boxes to be sampled from.
            gt_bboxes (Tensor): Ground truth bboxes.
            gt_labels (Tensor, optional): Class labels of ground truth bboxes.

        Returns:
        -------
            :obj:`SamplingResult`: Sampling result.
        """
        if len(bboxes.shape) < 2:
            bboxes = bboxes[None, :]

        bboxes = bboxes[:, :4]

        gt_flags = ops.zeros((bboxes.shape[0],), ms.uint8)
        if self.add_gt_as_proposals and gt_bboxes:
            if gt_labels is None:
                raise ValueError(
                    'gt_labels must be given when add_gt_as_proposals is True')
            bboxes = ops.concat([gt_bboxes, bboxes], dim=0)
            assign_result.add_gt_(gt_labels)
            gt_ones = ops.ones(gt_bboxes.shape[0], dtype=ms.uint8)
            gt_flags = ops.concat([gt_ones, gt_flags])

        num_expected_pos = int(self.num * self.pos_fraction)
        pos_inds = self._sample_pos(
            assign_result, num_expected_pos, bboxes=bboxes, **kwargs)
        if pos_inds.size > 0:
            pos_inds, _ = ops.unique(pos_inds)
        num_sampled_pos = pos_inds.size
        num_expected_neg = self.num - num_sampled_pos
        if self.neg_pos_ub >= 0:
            _pos = max(1, num_sampled_pos)
            neg_upper_bound = int(self.neg_pos_ub * _pos)
            if num_expected_neg > neg_upper_bound:
                num_expected_neg = neg_upper_bound
        neg_inds = self._sample_neg(
            assign_result, num_expected_neg, bboxes=bboxes, **kwargs)
        if neg_inds.size > 0:
            neg_inds, _ = ops.unique(neg_inds)

        sampling_result = SamplingResult(pos_inds, neg_inds, bboxes, gt_bboxes,
                                         assign_result, gt_flags)
        return sampling_result

# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
from mindspore import ops, nn

from ...config import Config
from .iou_loss import BoundedIoULoss
from .focal_loss import SigmoidFocalClassificationLoss


def create_loss_shape(config: Config) -> BoundedIoULoss:
    loss_shape = BoundedIoULoss(
        beta=config.beta,
        loss_weight=config.loss_weight,
    )
    return loss_shape


def create_loss_bbox(config: Config):
    if config.type == 'SmoothL1Loss':
        return nn.SmoothL1Loss(beta=config.get('beta', 1.0))
    if config.type == 'L1Loss':
        return nn.L1Loss()
    raise RuntimeError(f'Unknown type of loss_bbox: {config.type}')


class SigmoidCEWithLogits(nn.Cell):
    def construct(self, logits, labels, label_weights=None):
        loss_fn = nn.CrossEntropyLoss(weight=label_weights)
        return loss_fn(ops.sigmoid(logits), labels)


def create_loss_cls(config: Config):
    if config.type == 'CrossEntropyLoss':
        return SigmoidCEWithLogits()
    if config.type == 'FocalLoss':
        # return nn.FocalLoss(
        #     gamma=config.get('gamma', 2.0))
        return SigmoidFocalClassificationLoss(
            gamma=config.get('gamma', 2.0))
    raise RuntimeError(f'Unknown type of loss_cls: {config.type}')


def create_loss_loc(config: Config):
    return SigmoidFocalClassificationLoss(
        gamma=config.get('gamma', 2.0))

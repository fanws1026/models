
# Contents

- [Contents](#contents)
- [TAML Description](#TAML-description)
- [Dataset](#dataset)
- [Environment Requirements](#environment-requirements)
- [Quick Start](#quick-start)
- [Script Description](#script-description)
    - [Script and Sample Code](#script-and-sample-code)
    - [Script Parameters](#script-parameters)
    - [Training Process](#training-process)
        - [Training](#training)
    - [Evaluation Process](#evaluation-process)
        - [Evaluation](#evaluation)
    - [Inference Process](#inference-process)
        - [Export MindIR](#export-mindir)
- [Model Description](#model-description)
    - [Performance](#performance)
        - [Training Performance](#training-performance)
        - [Inference Performance](#inference-performance)
- [Description of Random Situation](#description-of-random-situation)
- [ModelZoo Homepage](#modelzoo-homepage)

# [TAML Description](#contents)

The proposed model, HTML, consists of HTKAR and Multi-Learner Network with Self-Distillation modules.
Specifically, HTKAR module is a hierarchical task-adaptive knowledge representation module which includes general experts to learn universal knowledge across tasks, task-specific experts to learn task-sensitive knowledge, learner-specific experts to further cover task-aware knowledge in different aspects, and adaptive gate to generate the input embedding for each learner from the outputs of general experts and experts of the corresponding task and learner. Multi-Learner Network with Self-Distillation module aggregate output logits of each learner and apply selk-distillation among them.

Task Adaptive Multi-learner Network for
Joint CTR and CVR Estimation

ACM2023

# [Dataset](#contents)

- [the Ali-CCP (Alibaba Click and Conversion Prediction) dataset](https://tianchi.aliyun.com/dataset/408)

# [Environment Requirements](#contents)

- Hardware（CPU）
    - Prepare hardware environment with CPU  processor.
- Framework
    - [MindSpore-2.0.0](https://www.mindspore.cn/install/en)
- Requirements
  - pandas
  - numpy
  - random
  - mindspre==2.0.0
  - tqdm
  - sklearn
  - joblib
- For more information, please check the resources below：
  - [MindSpore Tutorials](https://www.mindspore.cn/tutorials/en/r2.0/index.html)
  - [MindSpore Python API](https://www.mindspore.cn/docs/en/r2.0/index.html)

# [Quick Start](#contents)

After installing MindSpore via the official website, you can start training and evaluation as follows:

- processing dataset

  ```python
  # run processing
  python get_dataset.py
  ```

- running on CPU

  ```python
  # run training
  python train.py
  # run evaluation
  python eval.py
  ```

# [Script Description](#contents)

## [Script and Sample Code](#contents)

```bash
.
└─TAML
  ├─README.md             # descriptions of warpctc
  ├─eval.py               # model evaluation processing
  ├─get_dataset.py        # data process  
  ├─model.py              # TAML structure
  ├─model_config.py       # model training parameters
  ├─module.py             # modules in TAML
  ├─train.py              # train TAML
  ├─export.py             # export model to MindIR format
  └─requirements.txt      # model requirements
```

## [Script Parameters](#contents)

Parameters for both training and evaluation can be set in `model_config.py`

- Parameters for the Ali-CCP (Alibaba Click and Conversion Prediction) dataset

```python
bottom_set # bottom module parameters
bottom_set.expert_hidden_units = [64] # mlp units in expert
bottom_set.expert_keep_rate = 1 # 1-dropout in expert
bottom_set.expert_use_bn = False # batchnorm in expert
bottom_set.gate_hidden_units = [64] # mlp units in gate
bottom_set.gate_keep_rate = 1 # 1-dropout in gate
bottom_set.gate_use_bn = False # batchnorm in gate
bottom_set.feature_dims = 18 # size of input embedding
bottom_set.num_tasks = 2 # number of tasks
bottom_set.general_expert_nums = 1 # number of general expert
bottom_set.domain_expert_nums = 1 # number of domain expert for each task
bottom_set.learner_expert_nums = 1  # number of learner exbert for each learner
bottom_set.num_learners = 2 # number of learner for each task

tower_set # tower module parameters
tower_set.tower_hidden_units= [64, 32] # mlp units in learner
tower_set.tower_keep_rate = 1  # 1-dropout in learner
tower_set.tower_use_bn = False # batchnorm in learner

loss_set # loss parameters
task_relation = 'product-ctcvr' # relation between two tasks
stop_gd = False # stop gradient
t1_weight = 1.0 # weight for ctr task
t2_weight = 1.0 # weight for ctcvr task
t1_kd_weight = 0.1 # weight for ctr self-distillation
t2_kd_weight = 0.1 # weight for cvr self-distillation

epochs = 1 # training epoch
batchsize = 2048 # batchsize
lr = 1e-3 # learn rate
seed = 3047 # random seed
embedding_size = 8 # embedding dim for each feature
 ```

## [Training Process](#contents)

### Training

- running on CPU

  ```python
  python train.py
  ```

- The python command above will run in the terminal.

  ```txt
   1000it [01:44,  9.53it/s]
   ...
  ```

- The model checkpoint will be saved in the current directory.

## [Evaluation Process](#contents)

### Evaluation

- evaluation on dataset

  Before running the command below, please check the checkpoint path used for evaluation.

  ```python
  python eval.py
  ```

  The above python command will run in the terminal.

  ```txt
   Test:
    auc_click: 0.6779, auc_purchase: 0.6580, Avg loss: 0.215165
  ```

## Inference Process

**Before inference, please refer to [MindSpore Inference with C++ Deployment Guide](https://gitee.com/mindspore/models/blob/master/utils/cpp_infer/README.md) to set environment variables.**

### [Export MindIR](#contents)

- Export on local

  ```shell
  python export.py
  ```

# [Model Description](#contents)

## [Performance](#contents)

### Training Performance

| Parameters          | CPU                               |
|---------------------|-----------------------------------|
| Model Version       | TAML                              |
| Resource            | CPU 2.90GHz;16Core;32G Memory     |
| uploaded Date       | 07/27/2023 (month/day/year)       |
| MindSpore Version   | 2.0.0                             |
| Dataset             | [1]                               |
| Training Parameters | epoch=1, batch_size=2048, lr=1e-3 |
| Optimizer           | Adam                              |
| Loss Function       | Sigmoid Cross Entropy With Logits |
| outputs             | Loss                              |
| Loss                | 0.215                             |
| Per Step Time       | 104.93 ms                          |

### Inference Performance

| Parameters        | CPU                           |
|-------------------|-------------------------------|
| Model Version     | TAML                          |
| Resource          | CPU 2.90GHz;16Core;32G Memory |                        |
| Uploaded Date     | 07/27/2023 (month/day/year)   |
| MindSpore Version | 2.0.0                         |
| Dataset           | [1]                           |
| batch_size        | 2048                          |
| outputs           | AUC                           |
| AUC               | 0.658                         |

# [Description of Random Situation](#contents)

We set the random seed before training in model_config.py.

# [ModelZoo Homepage](#contents)

 Please check the official [homepage](https://gitee.com/mindspore/models)
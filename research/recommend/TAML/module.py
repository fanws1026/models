# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================

import mindspore as ms
from mindspore import nn
from mindspore.ops import expand_dims, cat


class TOWER(nn.Cell):
    """
    Muiti-Learner Module Structure
    """

    def __init__(self, tower_hidden_units, tower_keep_rate, tower_use_bn, feature_dims, num_learners):
        super().__init__()
        self.tower_hidden_units = tower_hidden_units
        self.tower_keep_rate = tower_keep_rate
        self.tower_use_bn = tower_use_bn
        self.feature_dims = feature_dims
        self.num_learners = num_learners

        self.tower_layers = nn.CellList([])
        for _ in range(self.num_learners):
            self.tower_layers.append(
                mlp_tower(self.tower_hidden_units, self.tower_keep_rate, self.tower_use_bn, self.feature_dims))

    def construct(self, x):
        tower_outputs = []

        # tower_nums*(None, hidden_units, 1)
        for index, tower_layer in enumerate(self.tower_layers):
            tower_output = tower_layer(x[index])
            tower_outputs.append(tower_output)

        return tower_outputs


class HTAKR(nn.Cell):
    """
    HTAKR Module Structure
    """

    def __init__(self, expert_hidden_units, expert_keep_rate, expert_use_bn, gate_hidden_units, gate_keep_rate,
                 gate_use_bn, feature_dims, general_expert_nums, domain_expert_nums, learner_expert_nums, num_tasks,
                 num_learners):
        super().__init__()
        self.expert_hidden_units = expert_hidden_units
        self.expert_keep_rate = expert_keep_rate
        self.expert_use_bn = expert_use_bn
        self.gate_hidden_units = gate_hidden_units
        self.gate_keep_rate = gate_keep_rate
        self.gate_use_bn = gate_use_bn
        self.feature_dims = feature_dims
        self.num_tasks = num_tasks

        self.num_learners = num_learners
        self.general_expert_nums = general_expert_nums
        self.domain_expert_nums = domain_expert_nums
        self.learner_expert_nums = learner_expert_nums

        self.num_towers = num_tasks * num_learners
        self.cur_expert_nums = general_expert_nums + domain_expert_nums + learner_expert_nums

        self.general_layers = nn.CellList([])
        for _ in range(self.general_expert_nums):
            self.general_layers.append(
                mlp(self.expert_hidden_units, self.expert_keep_rate, self.expert_use_bn, self.feature_dims))
        self.domain_layers = nn.CellList([])
        for _ in range(self.num_tasks * self.domain_expert_nums):
            self.domain_layers.append(
                mlp(self.expert_hidden_units, self.expert_keep_rate, self.expert_use_bn, self.feature_dims))
        self.learner_layers = nn.CellList([])
        for _ in range(self.num_tasks * self.learner_expert_nums * self.num_learners):
            self.learner_layers.append(
                mlp(self.expert_hidden_units, self.expert_keep_rate, self.expert_use_bn, self.feature_dims))
        self.gate_layers = nn.CellList([])
        for _ in range(self.num_towers):
            self.gate_layers.append(
                mlp_gate(self.gate_hidden_units, self.gate_keep_rate, self.gate_use_bn, self.feature_dims,
                         self.cur_expert_nums))

    def construct(self, x):
        general_outputs, domain_outputs, learner_outputs, gate_outputs, final_outputs = [], [], [], [], []

        # general_nums*(None, hidden_units, 1)
        for general_layer in self.general_layers:
            general_output = general_layer(x)
            general_output = expand_dims(general_output, axis=2)
            general_outputs.append(general_output)

        # domain_nums*(None, hidden_units, 1)
        for domain_layer in self.domain_layers:
            domain_output = domain_layer(x)
            domain_output = expand_dims(domain_output, axis=2)
            domain_outputs.append(domain_output)

        # learner_nums*(None, hidden_units, 1)
        for learner_layer in self.learner_layers:
            learner_output = learner_layer(x)
            learner_output = expand_dims(learner_output, axis=2)
            learner_outputs.append(learner_output)

        for gate_layer in self.gate_layers:
            gate_output = gate_layer(x)
            gate_output = expand_dims(gate_output, axis=1)
            gate_outputs.append(gate_output)

        for i, gate_output in enumerate(gate_outputs):
            task_id = i // self.num_learners

            tmp = []
            tmp.extend(general_outputs)
            tmp.extend(domain_outputs[task_id * self.domain_expert_nums: (task_id + 1) * self.domain_expert_nums])
            tmp.extend(learner_outputs[i * self.learner_expert_nums: (i + 1) * self.learner_expert_nums])
            task_output = cat(tmp, axis=2)
            weighted_expert_output = task_output * gate_output
            task_output = ms.ops.sum(weighted_expert_output, dim=2)
            final_outputs.append(task_output)

        return final_outputs


class mlp(nn.Cell):
    def __init__(self, hidden_units, keep_rate, _use_bn, feature_dims):
        super().__init__()
        self.hidden_units = hidden_units
        self.keep_rate = keep_rate
        self_use_bn = _use_bn
        self.feature_dims = feature_dims
        self.mlp_layers = nn.CellList([])
        for i, unit in enumerate(self.hidden_units):
            self.mlp_layers.append(nn.Dense(in_channels=self.feature_dims, out_channels=unit))
            self.feature_dims = self.hidden_units[i]
            if self_use_bn:
                self.mlp_layers.append(nn.BatchNorm1d(num_features=unit))
            if 1 - self.keep_rate:
                self.mlp_layers.append(nn.Dropout1d(keep_rate=self.keep_rate))
            self.mlp_layers.append(nn.ReLU())

    def construct(self, x):
        for layer in self.mlp_layers:
            x = layer(x)
        return x


class mlp_gate(nn.Cell):
    def __init__(self, hidden_units, keep_rate, _use_bn, feature_dims, cur_expert_nums):
        super().__init__()
        self.hidden_units = hidden_units
        self.keep_rate = keep_rate
        self_use_bn = _use_bn
        self.feature_dims = feature_dims
        self.cur_expert_nums = cur_expert_nums
        self.mlp_layers = nn.CellList([])
        for i, unit in enumerate(self.hidden_units):
            self.mlp_layers.append(nn.Dense(in_channels=self.feature_dims, out_channels=unit))
            self.feature_dims = self.hidden_units[i]
            if self_use_bn:
                self.mlp_layers.append(nn.BatchNorm1d(num_features=unit))
            if 1 - self.keep_rate:
                self.mlp_layers.append(nn.Dropout1d(keep_rate=self.keep_rate))
            self.mlp_layers.append(nn.ReLU())
        self.mlp_layers.append(
            nn.Dense(in_channels=self.feature_dims, out_channels=self.cur_expert_nums, activation='softmax'))

    def construct(self, x):
        for layer in self.mlp_layers:
            x = layer(x)
        return x


class mlp_tower(nn.Cell):
    def __init__(self, hidden_units, keep_rate, _use_bn, feature_dims):
        super().__init__()
        self.hidden_units = hidden_units
        self.keep_rate = keep_rate
        self_use_bn = _use_bn
        self.feature_dims = feature_dims
        self.mlp_layers = nn.CellList([])
        for i, unit in enumerate(self.hidden_units):
            self.mlp_layers.append(nn.Dense(in_channels=self.feature_dims, out_channels=unit))
            self.feature_dims = self.hidden_units[i]
            if self_use_bn:
                self.mlp_layers.append(nn.BatchNorm1d(num_features=unit))
            if 1 - self.keep_rate:
                self.mlp_layers.append(nn.Dropout1d(keep_rate=self.keep_rate))
            self.mlp_layers.append(nn.ReLU())
        self.mlp_layers.append(nn.Dense(in_channels=self.feature_dims, out_channels=1))

    def construct(self, x):
        for layer in self.mlp_layers:
            x = layer(x)
        return x

# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
import math
from mindspore import dtype as mdtype
from mindspore import nn, ms_function, numpy as mnp, ops, Parameter

class MultiHeadedAttention(nn.Cell):
    def __init__(self, head, hidden_sizes, dropout_rate, input_sizes=None):
        super(MultiHeadedAttention, self).__init__()
        if isinstance(hidden_sizes, int):
            hidden_sizes = [hidden_sizes] * 4
        if input_sizes is None:
            input_sizes = hidden_sizes
        for hidden_size in hidden_sizes:
            assert hidden_size % head == 0
        self.head = head
        self.head_size = hidden_sizes[0] // head
        self.hidden_size = hidden_sizes[-1]
        self.d_k = math.sqrt(hidden_sizes[0] // head)
        self.linear_s = nn.CellList(
            [nn.Dense(input_size, hidden_size) for (input_size, hidden_size) in zip(input_sizes, hidden_sizes)])
        self.dropout = nn.Dropout(p=dropout_rate)
        self.batch_matmul = ops.BatchMatMul(transpose_b=True)

    @ms_function
    def attention(self, query, key, value, mask=None):
        scores = ops.div(self.batch_matmul(query, key), self.d_k)
        if mask is not None:
            scores = scores.masked_fill(mask == 0, -1e9)
        p_attn = ops.softmax(scores, axis=-1)
        return ops.matmul(p_attn, value), p_attn

    def construct(self, query, key, value, mask=None):
        query, key, value = [
            l(x.view(-1, x.shape[-1])).view(*x.shape[:2], self.head, self.head_size).swapaxes(1, 2)
            for l, x in zip(self.linear_s, (query, key, value))]
        x, _ = self.attention(query, key, value, mask)  # (B, Head, L, D_H)
        x = x.swapaxes(1, 2)
        return self.linear_s[-1](x.view(-1, self.head * self.head_size)).view(*x.shape[:2], self.hidden_size)


class FeedForward(nn.Cell):
    def __init__(self, head, input_size, dropout_rate):
        super(FeedForward, self).__init__()
        self.mh = MultiHeadedAttention(head, input_size, dropout_rate)
        self.dropout1 = nn.Dropout(p=dropout_rate)
        self.dropout2 = nn.Dropout(p=dropout_rate)
        self.activate = nn.LeakyReLU()
        self.ln1 = nn.LayerNorm((input_size,))
        self.ln2 = nn.LayerNorm((input_size,))
        self.fc1 = nn.Dense(input_size, input_size)
        self.fc2 = nn.Dense(input_size, input_size)

    def construct(self, s, mask):
        s = s + self.dropout1(self.mh(s, s, s, mask))
        s = self.ln1(s)
        s_ = self.activate(self.fc1(s))
        s_ = self.dropout2(self.fc2(s_))
        s = self.ln2(s + s_)
        return s


class Transformer(nn.Cell):
    def __init__(self, input_size, hidden_size, dropout_rate, head=1, b=1, position=False, transformer_mask=True):
        super(Transformer, self).__init__()
        self.position = position
        if position:
            self.pe = PositionalEncoding(input_size, 0.5)
        self.fc = nn.Dense(input_size, hidden_size)
        self.SAs = nn.CellList([MultiHeadedAttention(head, hidden_size, dropout_rate) for _ in range(b)])
        self.FFNs = nn.CellList([FeedForward(head, hidden_size, dropout_rate) for _ in range(b)])
        self.b = b
        self.transformer_mask = transformer_mask

    def construct(self, inputs, mask=None):
        if self.position:
            inputs = self.pe(inputs)
        inputs = self.fc(inputs)
        max_len = inputs.shape[1]
        if self.transformer_mask:
            mask = mnp.tril(ops.ones((1, max_len, max_len), mdtype.bool_))
        # elif mask is not None:
        #     mask = ops.expand_dims(mask, 1)  # (B, 1, L)
        if mask is not None:
            mask = ops.expand_dims(mask, 1)  # For head, shape is (B, 1, L, L) or (B, 1, 1, L)
        for i in range(self.b):
            inputs = self.SAs[i](inputs, inputs, inputs, mask)
            inputs = self.FFNs[i](inputs, mask)
        return inputs


class PositionalEncoding(nn.Cell):
    """Implement the PE function."""

    def __init__(self, d_model, dropout, max_len=1000):
        super(PositionalEncoding, self).__init__()
        self.dropout = nn.Dropout(p=dropout)

        pe = mnp.zeros((max_len, d_model))
        position = mnp.arange(0, max_len).expand_dims(1)
        div_term = ops.exp(mnp.arange(0, d_model, 2) *
                           -(math.log(10000.0) / d_model))
        pe[:, 0::2] = ops.sin(position * div_term)
        pe[:, 1::2] = ops.cos(position * div_term)[:, :d_model // 2]
        pe = pe.expand_dims(0)
        self.pe = Parameter(pe, requires_grad=False)

    def construct(self, x):
        x = x + self.pe[:, :x.shape[1]]
        return self.dropout(x)

# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
import copy as cp
from collections import defaultdict
import gc
import numpy as np
from sklearn.metrics import roc_auc_score
import mindspore as ms
from mindspore import ops, dataset as ds, numpy as mnp
from categorical import Categorical
from .dataset import Dataset

class Memory:
    def __init__(self):
        self.actions = []
        self.states = []
        self.logprobs = []
        self.rewards = []
        self.action_masks = []
        self.dones = []

    def clear_memory(self):
        del self.actions[:]
        del self.states[:]
        del self.logprobs[:]
        del self.rewards[:]
        del self.action_masks[:]
        del self.dones[:]

class GCATAgent:
    def __init__(self, env, fa, args):
        self.env = env
        self.fa = fa
        self.args = args
        self.memory = Memory()
        self.all_AUC, self.all_ACC = defaultdict(list), defaultdict(list)
        self.best_AUC, self.best_ACC = None, None
        self.best_value = -1
        self.all_cov = defaultdict(list)
        self.know_map = env.know_map

    def train(self):
        for epoch in range(self.args.training_epoch):
            self.collecting_data_update_model("training", epoch)
            # self.collecting_data_update_model("validation", epoch)
            for tmp in range(3):
                self.env.re_split_data(_id=tmp+100)
                self.collecting_data_update_model("evaluation", epoch)
            # average evaluations
            print("Mean Metrics: ")
            for step in self.args.ST:
                print(str(step) + 'AUC: ' + str(round(np.mean(self.all_AUC[step]), 4)) \
                                 + ", ACC: " + str(round(np.mean(self.all_ACC[step]), 4)))
            for step in self.args.ST:
                tmp_auc = ','.join([str(x) for x in self.all_AUC[step]])
                tmp_acc = ','.join([str(x) for x in self.all_ACC[step]])
                print(str(step) + 'AUC: ' + tmp_auc + ", ACC: " + tmp_acc)
            # compare on AUC
            if np.mean(self.all_AUC[self.args.ST[-1]]) > self.best_value:
                self.best_value = np.mean(self.all_AUC[self.args.ST[-1]])
                self.best_AUC, self.best_ACC = self.all_AUC, self.all_ACC
            # best metric
            print("Best Mean Metrics: ")
            for step in self.args.ST:
                print(str(step) + 'AUC: ' + str(round(np.mean(self.all_AUC[step]), 4)) \
                                 + ", ACC: " + str(round(np.mean(self.all_ACC[step]), 4)))
            # empty
            self.all_AUC, self.all_ACC = defaultdict(list), defaultdict(list)
            # re-split
            self.env.re_split_data(_id=None)
        print('\n'.join([str(round(np.mean(self.all_cov[i]), 4)) for i in range(self.args.T)]))
        self.all_cov = defaultdict(list)

    def collecting_data_update_model(self, flag, epoch):
        if flag == "training":
            dataset = Dataset(self.env.get_records('training'), self.env.item_num)
            loader = ds.GeneratorDataset(dataset, column_names=['user_ids', 'mask'], shuffle=True) \
                            .batch(batch_size=self.args.train_bs, num_parallel_workers=3)
        elif flag == "validation":
            dataset = Dataset(self.env.get_records('validation'), self.env.item_num)
            loader = ds.GeneratorDataset(dataset, column_names=['user_ids', 'mask'], shuffle=False) \
                            .batch(batch_size=self.args.test_bs, num_parallel_workers=3)
        elif flag == "evaluation":
            dataset = Dataset(self.env.get_records('evaluation'), self.env.item_num)
            loader = ds.GeneratorDataset(dataset, column_names=['user_ids', 'mask'], shuffle=False) \
                            .batch(batch_size=self.args.test_bs, num_parallel_workers=3)
        else:
            print('Data flag no exist')
            exit()

        infos = {step: [] for step in self.args.ST}
        all_loss = 0
        tqdm_obj = loader.create_dict_iterator()
        for batch in tqdm_obj:
            all_loss = self.train_batch(flag, epoch, infos, all_loss, batch)
        if flag == 'training':
            print('policy loss: ' + str(np.round(all_loss/len(loader), 4)))
        # compute AUC ACC
        if flag != "training":
            for step in self.args.ST:
                pred, label = [], []
                for metric in infos[step]:
                    pred.append(metric['pred'])
                    label.append(metric['label'])
                pred = np.concatenate(pred, axis=0)
                label = np.concatenate(label, axis=0)
                pred_bin = np.where(pred > 0.5, 1, 0)
                ACC = np.sum(np.equal(pred_bin, label)) / len(pred_bin)
                try:
                    AUC = roc_auc_score(label, pred)
                except ValueError:
                    AUC = -1
                self.all_ACC[step].append(ACC)
                self.all_AUC[step].append(AUC)

        print(f'epoch: {epoch}, flag: {flag}')

    def train_batch(self, flag, epoch, infos, all_loss, batch):
        selected_users = batch['user_ids']
        state = self.env.reset_with_users(selected_users.asnumpy())
        action_mask = batch['mask']
        done = False
        cnt_step = 0
        while not done:
            batch_question = cp.deepcopy(state['batch_question'])
            batch_answer = cp.deepcopy(state['batch_answer'])
            pnt = [cnt_step for _ in range(len(selected_users))]
            batch_know, batch_know_num = self.get_know_num(batch_question)
            data = {"p_rec": batch_question, "p_t": pnt, \
                        "a_rec": batch_answer, 'kn_rec': batch_know, 'kn_num': batch_know_num}
            logits = self.fa.predict(data)
            # add mask
            inf_mask = ops.clip_by_value(ops.log(action_mask.astype(ms.float32)), \
                                            clip_value_min=ms.Tensor(np.finfo(np.float32).min))
            logits = logits + inf_mask
            action_probs = ops.softmax(logits, axis=-1)
            dist = Categorical(action_probs)
            action = dist.sample() # sample, not for max
            if flag == 'training':
                self.memory.actions.append(action)
                self.memory.logprobs.append(dist.log_prob(action))
                self.memory.action_masks.append(cp.deepcopy(action_mask))
                self.memory.states.extend([(batch_question[i], pnt[i], batch_answer[i]) \
                                        for i in range(len(selected_users))])
            # update mask
            action_mask[mnp.arange(len(action_mask)).expand_dims(1), action.expand_dims(1)] = 0
            action = action.asnumpy()
            state_next, rwd, done, all_info, cov = self.env.step(action)
            if (epoch == self.args.training_epoch-1) and flag == 'evaluation':
                self.all_cov[cnt_step].append(cov)
            if flag == 'training':
                self.memory.rewards.append(rwd)
                self.memory.dones.append(done)
            state = state_next
            cnt_step += 1
            if cnt_step in self.args.ST:
                infos[cnt_step].extend(all_info)
        # train policy
        if flag == "training":
            rewards = []
            discounted_reward = 0
            for reward, is_terminal in zip(reversed(self.memory.rewards), reversed(self.memory.dones)):
                if is_terminal:
                    discounted_reward = 0
                discounted_reward = reward + (self.args.gamma * discounted_reward)
                rewards.insert(0, discounted_reward)
            rewards = np.concatenate(rewards, axis=0)
            rewards = ms.Tensor(rewards, dtype=ms.float32)
            old_actions = ops.concat(self.memory.actions, axis=0)
            old_logprobs = ops.concat(self.memory.logprobs, axis=0)
            old_actionmask = ops.concat(self.memory.action_masks, axis=0)
            ep_length = len(old_actions)
            batch_size = ep_length//4
            mini_batch_indicies = [list(range(ep_length))[i:i + batch_size] \
                                    for i in range(0, ep_length, batch_size)]
            for indices in mini_batch_indicies:
                b_rewards = rewards[indices]
                b_old_states = self.convert_state([self.memory.states[idx] for idx in indices])
                b_old_actions = old_actions[indices]
                b_old_logprobs = old_logprobs[indices]
                b_old_actionmask = old_actionmask[indices]
                loss = self.fa.optimize_model(b_old_states, b_old_actions, \
                                                b_old_logprobs, b_old_actionmask, b_rewards)
                ms.ms_memory_recycle()
            all_loss = all_loss + loss
            self.fa.transfer_weights()
        # reset student
        self.env.model.init_stu_emb()
        # clear memory
        self.memory.clear_memory()
        gc.collect()

        return all_loss

    def convert_state(self, batch):
        p_r = np.array([item[0] for item in batch])
        pnt = np.array([item[1] for item in batch])
        a_rec = np.array([item[2] for item in batch])
        batch_know, batch_know_num = self.get_know_num(p_r)
        data = {"p_rec": p_r, "p_t": pnt, "a_rec": a_rec, \
                    'kn_rec': batch_know, 'kn_num': batch_know_num}
        return data

    def get_know_num(self, batch_question):
        batch_know, batch_know_num = [], []
        for i in range(len(batch_question)):
            concepts_embs, concepts_num = [], []
            for qid in batch_question[i]:
                concepts_emb = [0.] * self.env.know_num
                concepts = self.env.know_map[qid]
                for concept in concepts:
                    concepts_emb[concept] = 1.0
                concepts_embs.append(concepts_emb)
                concepts_num.append(1 if qid == 0 else len(self.env.know_map[qid]))
            batch_know.append(ms.Tensor(concepts_embs))
            batch_know_num.append(ms.Tensor(concepts_num))
        batch_know = ops.stack(batch_know)
        batch_know_num = ops.stack(batch_know_num)
        return batch_know, batch_know_num

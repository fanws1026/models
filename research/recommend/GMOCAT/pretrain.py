# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
import os
import json
import sys
import random
import time
from mindspore import context
from util import set_global_seeds, arg_parser, check_path

from environments.dataset import TrainDataset
from environments.irt import IRTModel

def common_arg_parser():
    parser = arg_parser()
    parser.add_argument('-seed', type=int, default=145)
    parser.add_argument('-data_name', type=str, default='assist2009')
    parser.add_argument('-CDM', type=str, default='IRT')
    parser.add_argument('-T', type=int, default=20, help='time step')
    parser.add_argument('-gpu_no', type=str, default="0", help='which gpu for usage')
    parser.add_argument('-learning_rate', type=float, default=0.002, help="learning rate")
    parser.add_argument('-training_epoch', type=int, default=50, help="training epoch")
    parser.add_argument('-batch_size', type=int, default=2048, help="batch_size")
    return parser

def main(args):
    arg_parser = common_arg_parser()
    args, _ = arg_parser.parse_known_args(args)
    args.path = "_".join([args.CDM, args.data_name, str(args.T)])
    set_global_seeds(args.seed)
    os.environ["CUDA_VISIBLE_DEVICES"] = str(args.gpu_no)

    context.set_context(mode=context.PYNATIVE_MODE, device_target='CPU')
    device_target = context.get_context('device_target')
    mode = context.get_context('mode')
    print(device_target, mode)

    print("Pre Training CDM: " + args.path)

    with open(f'data/concept_map_{args.data_name}.json', encoding='utf8') as i_f:
        concept_data = json.load(i_f)
    with open(f'data/train_task_{args.data_name}.json', encoding='utf8') as i_f:
        stus = json.load(i_f)

    rates = {}
    items = set()
    user_cnt = -1
    know_map = {}
    for stu in stus:
        user_cnt += 1
        rates[user_cnt] = {}
        for qid, label in zip(stu['q_ids'], stu['labels']):
            rates[user_cnt][int(qid)+1] = int(label)
            items.add(int(qid)+1)
            know_map[int(qid)+1] = concept_data[str(qid)]

    max_itemid = max(items)
    knows = set()
    for know_list in know_map.values():
        knows.update(know_list)
    max_knowid = max(knows)
    user_num = len(rates)
    item_num = max_itemid + 1
    know_num = max_knowid + 1
    print(f"user num: {user_num}, item num: {item_num}, know_num: {know_num}")

    N = user_num//10
    test_fold, valid_fold = 0, 1
    all_users = [idx for idx in range(user_num)]
    random.Random(args.seed).shuffle(all_users)

    train_users = [uu for i, uu in enumerate(all_users) \
                if i //N != test_fold and i//N != valid_fold]

    print(f'use user num: {len(train_users)}')
    records = []
    for uu in train_users:
        u_record = [(uu, pair[0], pair[1]) for pair in list(rates[uu].items())]
        records.extend(u_record)
    dataset = TrainDataset(records, know_map, user_num, item_num, know_num)

    name = args.CDM
    if name == 'IRT':
        cdm = IRTModel(args, user_num, item_num, 1)
    else:
        print("CDM no exist")
        exit()
    print("Hype-Parameters: " + str(args))
    check_path('models/{}/'.format(args.data_name))
    path = 'models/{}/{}.ckpt'.format(args.data_name, args.path)
    beg = time.time()
    cdm.train(dataset, args.learning_rate, args.batch_size, epochs=args.training_epoch, path=path)
    print(time.time()-beg)

if __name__ == '__main__':
    main(sys.argv)

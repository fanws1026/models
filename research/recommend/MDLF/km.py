# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================

from src.model_utils.config import config
from src.load_dataset import create_dataset
from src.MDLFMetric import MDLFMetric

from tqdm import tqdm
import mindspore as ms
import mindspore.ops as ops
import numpy as np


class KaplanMeier:
    def __init__(self, K, N):
        self.survival_num = ms.Tensor(np.zeros([K, N]), dtype=ms.float32)
        self.dead_num = ms.Tensor(np.zeros([K, N]), dtype=ms.float32)
        self.K = K
        self.N = N
        self.n_range = ops.Range()(ms.Tensor(0, ms.int32), ms.Tensor(N, ms.int32),
                                   ms.Tensor(1, ms.int32)).reshape(1, -1)
        self.k_range = ops.Range()(ms.Tensor(0, ms.int32), ms.Tensor(K, ms.int32),
                                   ms.Tensor(1, ms.int32)).reshape(1, -1)

    def update(self, score, pos):
        batch = len(score)
        score_mask = ops.cast(score.reshape(-1, 1) >= self.n_range, ms.float32)  # [B, N]
        self.survival_num += ops.ReduceSum()(score_mask, 0).reshape(-1, self.N)
        pos_mask = (pos.reshape(-1, 1) <= self.k_range).reshape(-1, self.K, 1).broadcast_to((-1, self.K, self.N))
        pos_mask = ops.logical_and(pos_mask, ops.equal(score.reshape(-1, 1, 1).broadcast_to((batch, self.K, self.N)),
                                                       self.n_range.reshape(1, 1, -1).broadcast_to(
                                                           (batch, self.K, self.N))))
        self.dead_num += ops.ReduceSum(keep_dims=False)(pos_mask, 0)

    def eval(self):
        survival_rates = ops.CumProd()(1 - self.dead_num / self.survival_num, 1)
        dead_rates = 1 - survival_rates
        return dead_rates


def run_train():
    """train function"""
    ds_train = create_dataset(config.mindrecord_path, config.dataset, config.batch_size, training=True,
                              target=config.device_target)
    ds_eval = create_dataset(config.mindrecord_path, config.dataset, config.batch_size,
                             training=False, target=config.device_target)

    if ds_train.get_dataset_size() == 0:
        raise ValueError(
            "Please check dataset size > 0 and batch_size <= dataset size.")
    print("create dataset finished.")

    step_per_size = ds_train.get_dataset_size()
    print("train dataset size:", step_per_size)

    train_data_iter = ds_train.create_dict_iterator()
    test_data_iter = ds_eval.create_dict_iterator()
    km = KaplanMeier(config.length, config.intervals)
    for _, data in enumerate(tqdm(train_data_iter)):
        cur_pos, cur_score = data['position_labels'], data['score_labels']
        cur_score = ops.clip_by_value((cur_score / config.interval_length).astype(ms.int32), ms.Tensor(0),
                                      ms.Tensor(config.intervals - 1))
        cur_pos = ops.clip_by_value(cur_pos, ms.Tensor(0), ms.Tensor(config.length)).astype(ms.int32)
        km.update(cur_score, cur_pos)

    print('Estimation done')
    cdf = km.eval()
    pdf = ops.Concat(axis=1)([cdf[:, 0:1], cdf[:, 1:] - cdf[:, :-1]])
    winning_rates = ops.Concat(axis=0)([cdf[0:1,], cdf[1:, :] - cdf[:-1, :]])

    metric = MDLFMetric(config.intervals, config.length, config.interval_length)
    for _, data in enumerate(tqdm(test_data_iter)):
        tot_pos = data['position_labels']
        tot_score = data['score_labels']
        metric.update(pdf, cdf, winning_rates, tot_pos, tot_score)
    metric.summary_print()


if __name__ == '__main__':
    run_train()

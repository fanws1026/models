# [Contents](#contents)

- [MDLF](#mdlf)

- [Dataset](#dataset)

- [Environment Requirements](#environment-requirements)

- [Script Description](#script-description)

- [Quick Start](#quick-start)

    - [Data Process](#data-process)
    - [Training Process](#training-process)
    - [Evaluation Process](#evaluatio-process)

- [Model Description](#model-description)

- [Description of Random Situation](#description-of-random-situation)

- [ModelZoo Homepage](#ModelZooHomepage)

# [MDLF](#contents)

Landscape forecasting is one of the key components in RTB, with the goal of predicting the highest bid price and the winning rate for a given bid price in an auction. It has been demonstrated to be essential for bid optimization in current works. This is the first work to address the landscape forecasting problem in the *multi-slot* context.

Specifically, this repo contains the implementation of MDLF in the following paper and some baselines for Multi-Slot Deep Landscape Forecasting:

[Paper](https://doi.org/10.1145/3580305.3599799): Weitong Ou, Bo Chen, Yingxuan Yang, Xinyi Dai, Weiwen Liu, Weinan Zhang, Ruiming Tang, Yong Yu. Deep Landscape Forecasting in Multi-Slot Real-Time Bidding.

# [Dataset](#contents)

As far as we are concerned, there is no publicly available dataset for multi-slot advertising that includes essential data like bids. Therefore, the experiments are conducted on the following two semi-synthetic bidding datasets.

- [iPinYou RTB dataset](https://github.com/wnzhang/make-ipinyou-data). Please follow the given link to formalize iPinYou RTB dataset.

- [Criteo Attribution Bidding Dataset](https://ailab.criteo.com/criteo-attribution-modeling-bidding-dataset/).

- Diemert E, Meynet J, Galland P, et al. Attribution modeling increases efficiency of bidding in display advertising[M] // Proceedings of the ADKDD'17. 2017: 1-6.

# [Environment Requirements](#contents)

- Hardware（Ascend/GPU）
    - Prepare hardware environment with Ascend or GPU processor.

- Framework
    - [MindSpore](https://www.mindspore.cn/install/en)

- For more information, please check the resources below：
    - [MindSpore Tutorials](https://www.mindspore.cn/tutorials/en/master/index.html)
    - [MindSpore Python API](https://www.mindspore.cn/docs/en/master/api_python/mindspore.html)

# [Script Description](#contents)

```text
.
└─MDLF
  ├─README.md                         # descriptions
  ├─src
    ├─configs                         # directory for parameter config file for reference.
    ├─model_utils
      ├─config.py                     # parsing parameter configuration file of "*.yaml"
      ├─device_adapter.py             # local or ModelArts training
      ├─local_adapter.py              # get related environment variables in local training
      └─moxing_adapter.py             # get related environment variables in ModelArts training
    ├─models
      ├─__init__.py                   # python init file
      ├─ADM.py                        # baseline: ADM network
      ├─DeepHit.py                    # baseline: DeepHit network
      ├─DWPP.py                       # baseline: DWPP network
      ├─Hierarch.py                   # baseline: Hierarch network
      ├─loss.py                       # define loss function
      └─MDLF.py                       # MDLF network and DLF network
    ├─__init__.py                     # python init file
    ├─callback.py                     # define callback function
    ├─data_preprocess.py              # code for data preprocessing
    ├─load_dataset.py                 # code for loading dataset
    ├─model_builder.py                # code for building network
    └─MDLFMetric.py                     # code for customized metric
  ├─eval.py                           # eval script
  ├─km.py                             # baseline: KM
  └─train.py                          # train script
```

# [Quick Start](#contents)

## [Data Process](#contents)

Remember to modify `source_data_path` and `mindrecord_path` in the config file, where `source_data_path` is the directory for the corresponding dataset and `mindrecord_path` is the directory to store the preprocessed mindrecord data.  

(Note that this script does not contain the formalization code for the iPinYou dataset. Please follow the official [link](https://github.com/wnzhang/make-ipinyou-data) to formalize it in advance.)

```shell
# for Criteo dataset
python src/data_preprocess.py --config_path=src/configs/configs_criteo_km.yaml

# for iPinYou dataset
python src/data_preprocess.py --config_path=src/configs/configs_ipinyou_km.yaml
```

## [Training Process](#contents)

KM baseline is a statistical method and does not need training.

For the other methods, run the following command to start training:

```shell
python train.py --config_path=src/configs/config_file_for_your_model.yaml
```

For each training, there are following files in the folder `output_path_in_config_file/Run_ID` (where the Run_ID is the datetime when this script starts):

- Some intermediary checkpoint files named like `algo-epoch_step`, e.g. `ADM-1_3962.ckpt`

- The best checkpoint files named as `best.ckpt`.

- The parameter configuration for this running: `config.yaml`

- The evaluation metrics when training are saved in `Eval_log`

- The loss values are saved in `Loss_log`

## [Evaluation Process](#Contents)

- For KM baseline, run the following

  ```shell
  python km.py --config_path=src/configs/config_ipinyou_km.py
  python km.py --config_path=src/configs/config_criteo_km.py
  ```

- For other methods, first check the directory `dir`  for the checkpoint file (in the form of  `output_path_in_config_file/Run_ID`)

  ```shell
  python eval.py --config_path=dir/config.yaml --ckpt_path=dir/best.ckpt
  ```

  The results will be printed.

# [Model Description](#contents)

## Training Performance

The default training parameters can be found in the config yaml in `src/configs`.

## Inference Performance

The inference performance in Criteo dataset:

| Methods  | MAE    | ANLP_PDF | ANLP_WR | C_GLOBAL | C_LOCAL |
| -------- | ------ | -------- | ------- | -------- | ------- |
| KM       | 0.2759 | 3.9958   | 2.5717  | -        | 1.0000  |
| DeepHit  | 0.1731 | 3.0200   | 2.2101  | 0.8494   | 0.9739  |
| Hierarch | 0.1746 | 3.0365   | 1.6165  | 0.8479   | 0.9999  |
| DWPP     | 0.1749 | 3.3262   | 1.7272  | 0.8413   | 0.9923  |
| ADM      | 0.1804 | 3.2369   | 2.3975  | 0.8419   | 0.9739  |
| DLF      | 0.1729 | 3.0184   | 1.6169  | 0.8489   | 0.9993  |
| MDLF     | 0.1722 | 3.0137   | 1.4852  | 0.8501   | 1.0000  |

The inference performance in iPinYou dataset:

| Methods  | MAE    | ANLP_PDF | ANLP_WR | C_GLOBAL | C_LOCAL |
| -------- | ------ | -------- | ------- | -------- | ------- |
| KM       | 0.2706 | 5.3749   | 2.5484  | -        | 1.0000  |
| DeepHit  | 0.1626 | 4.4456   | 1.9606  | 0.8146   | 0.9976  |
| Hierarch | 0.1654 | 4.3782   | 1.8850  | 0.7971   | 0.9988  |
| DWPP     | 0.1825 | 5.2665   | 3.5883  | 0.7557   | 0.9887  |
| ADM      | 0.1636 | 4.4017   | 1.9005  | 0.8088   | 0.9987  |
| DLF      | 0.1635 | 4.4124   | 2.7780  | 0.8060   | 0.9918  |
| MDLF     | 0.1640 | 4.3395   | 1.6896  | 0.8185   | 1.0000  |

# [Description of Random Situation](#contents)

The datasets are semi-synthesized.  We set the random seed before data preprocessing in `data_preprocess.py` and before training in `train.py`.

# [ModelZoo Homepage](#contents)

 Please check the official [homepage](https://gitee.com/mindspore/models).

# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================

import mindspore as ms
import mindspore.nn as nn
import mindspore.ops as ops


class Hierarch(nn.Cell):
    def __init__(self, feature_num, embedding_size=8, intervals=100, length=5, MAX_SPARSE=65000):
        super(Hierarch, self).__init__()
        self.embedding_size = embedding_size
        self.embeddings = nn.Embedding(MAX_SPARSE, self.embedding_size, embedding_table='normal')
        self.K = length
        self.N = intervals
        self.feature_num = feature_num
        self.share_layers = nn.SequentialCell(
            nn.Flatten(),
            nn.Dense(self.feature_num * self.embedding_size, 32),
            nn.ReLU()
        )
        self.event_layers = nn.CellList()
        self.layer_size = [1, 10, self.N // 10]
        if self.N % 10 != 0:
            raise ValueError('Invalid divisions!')
        for _ in range(self.K):
            event = nn.CellList()
            tot_size = 1
            for j in range(len(self.layer_size)):
                for _ in range(tot_size):
                    event.append(nn.Dense(32, self.layer_size[j]))
                tot_size *= self.layer_size[j]
            self.event_layers.append(event)

    def construct(self, x):
        embs = self.embeddings(x)
        shared_output = self.share_layers(embs)
        batch = x.shape[0]
        event_output = []
        for i in range(self.K):
            output = ops.Ones()((batch, self.N), ms.float32)
            tot_size = 1
            start = 0
            for j in range(len(self.layer_size)):
                layer_out = []
                for k in range(tot_size):
                    cur_out = self.event_layers[i][start + k](shared_output)
                    if j == 0:
                        layer_out.append(ops.repeat_elements(ops.Sigmoid()(cur_out),
                                                             self.N // (tot_size * self.layer_size[j]), 1))
                    else:
                        layer_out.append(ops.repeat_elements(ops.Softmax(axis=1)(cur_out),
                                                             self.N // (tot_size * self.layer_size[j]), 1))
                start += tot_size
                tot_size *= self.layer_size[j]
                output *= ops.Concat(axis=1)(layer_out)
            event_output.append(output)
        pdf = ops.Stack(axis=1)(event_output)
        cdf = ops.CumSum()(pdf, 2)
        winning_rate = ops.Concat(axis=1)((cdf[:, 0:1, :], cdf[:, 1:, :] - cdf[:, :-1, :]))
        return pdf, cdf, winning_rate

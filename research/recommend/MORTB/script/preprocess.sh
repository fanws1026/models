#!/bin/bash
Dataset_Path=$1 # the directory that contains the raw data file criteo_attribution_dataset.tsv.gz
Model_Path=$2
Data_path=$3 # the directory that stores the list data

# Step 1: Preprocess data to train CTR/CVR model
python data_preparation.py --mode preprocess --data_path $Dataset_Path

# Step 2: Train CTR and CVR model
python data_preparation.py --mode train --data_path $Dataset_Path --model_path $Model_Path --task ctr --lr=5e-5
python data_preparation.py --mode train --data_path $Dataset_Path --model_path $Model_Path --task cvr --lr=1e-4

# Optional Step
# python data_preparation.py --mode test --data_path $Dataset_Path --model_path $Model_Path/ctr_best.ckpt --task ctr
# python data_preparation.py --mode test --data_path $Dataset_Path --model_path $Model_Path/cvr_best.ckpt --task cvr

# Step 3: Generate lists for bidding in the multi-slot environment
python data_preparation --mode generate --ctr_model_path $Model_Path/ctr_best.ckpt --cvr_model_path $Model_Path/cvr_best.ckpt --output_path $Data_path

#!/bin/bash
# When testing, the lambda should be provided.
DataPath=$1  # The directory that stores the list data.
OutputPath=$2             # Log directory
lda=$3
CampaignID=10341182
StartDay=25
EndDay=30
Method=M_ORTB
length=5
budget=1
gamma=1

python -u main.py --campaign $CampaignID --start_day $StartDay --end_day $EndDay --lda $lda\
    --method $Method --data_path $DataPath --output_path $OutputPath --length $length --budget $budget --gamma $gamma \
    --mode test
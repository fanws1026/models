# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================

import argparse
import os
import pathlib
from copy import deepcopy

import numpy as np

from alternate_train import alternate_run

wandb = None


def run_params(args):
    params = deepcopy(vars(args))
    params["model"] = "MLP_SIG"
    if args.data_cache_path != "None":
        pathlib.Path(args.data_cache_path).mkdir(parents=True, exist_ok=True)

    if args.method == "ULC":
        params["loss"] = "cross_entropy_loss"
        params["dataset"] = "last_30_train_test_fsiw_next" + "_end_" + str(args.training_end_day) + "_seed_" + str(
            args.seed)

    return params


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--method", choices=["ULC"],
                        type=str, required=True)
    parser.add_argument("--seed", type=int, default=0)
    parser.add_argument("--dataset_source", type=str, default="criteo", choices=["criteo"])
    parser.add_argument("--CD", type=int, default=7,
                        help="interval between counterfactual deadline and actual deadline")
    parser.add_argument("--lr", type=float, default=1e-3)
    parser.add_argument("--data_path", type=str, default="./data/data.txt",
                        help="path of the data.txt in criteo dataset")
    parser.add_argument("--data_cache_path", type=str, default="./data")
    parser.add_argument("--batch_size", type=int,
                        default=1024)
    parser.add_argument("--epoch", type=int, default=5,
                        help="training epoch of pretraining")
    parser.add_argument("--l2_reg", type=float, default=0,
                        help="l2 regularizer strength")
    parser.add_argument("--training_end_day", type=int, default=58,
                        help="deadline for training data")
    parser.add_argument("--training_duration", type=int, default=21,
                        help="duration of training data")
    parser.add_argument("--valid_test_size", type=float, default=1,
                        help="duration of valid/test data")
    parser.add_argument("--train_epoch", type=int, default=100,
                        help="max train epoch")
    parser.add_argument("--early_stop", type=int, default=4)
    parser.add_argument("--cuda_device", type=str, default="0")
    parser.add_argument("--optimizer", type=str, default="Adam")
    parser.add_argument("--save_model", type=int, default=0)
    parser.add_argument("--base_model", type=str, default="MLP", choices=["MLP"])

    args = parser.parse_args()
    params = run_params(args)
    os.environ["CUDA_VISIBLE_DEVICES"] = params["cuda_device"]
    np.random.seed(args.seed)

    alternate_run(params, wandb)

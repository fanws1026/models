# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================

import mindspore.numpy as np
import mindspore.ops as ops
import mindspore


def stable_log1pex(x):
    return -np.where(x < 0, x, np.zeros_like(x)) + np.log(1 + np.exp(-np.absolute(x)))

def cross_entropy_loss(targets, outputs, params=None):
    z = targets["label"]
    x = outputs
    x = ops.Reshape()(x, (-1,))
    z = z.float()
    loss_value = ops.binary_cross_entropy_with_logits(x, z, mindspore.Tensor([1.0]), mindspore.Tensor([1.0]))

    return {"loss": loss_value}

def get_loss_fn(name):
    if name == "cross_entropy_loss":
        return cross_entropy_loss
    raise NotImplementedError("{} loss does not implemented".format(name))

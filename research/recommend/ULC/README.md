
# Contents

- [Contents](#contents)
- [UCL Description](#TAML-description)
- [Dataset](#dataset)
- [Environment Requirements](#environment-requirements)
- [Quick Start](#quick-start)
- [Script Description](#script-description)
    - [Script and Sample Code](#script-and-sample-code)
    - [Training Process](#training-process)
        - [Training](#training)
- [ModelZoo Homepage](#modelzoo-homepage)

# [ULC Description](#contents)

Conversion rate prediction is critical to many online applications such as digital display advertising. To capture
dynamic data distribution, industrial systems often require retraining models on recent data daily or weekly. However,
the delay of conversion behavior usually leads to incorrect labeling, which is called delayed feedback problem. Existing
work may fail to introduce the correct information about false negative samples due to data sparsity and dynamic data
distribution. To directly introduce the correct feedback label information, we propose an Unbiased delayed feedback
Label Correction framework (ULC), which uses an auxiliary model to correct labels for observed negative feedback
samples. Firstly, we theoretically prove that the label-corrected loss is an unbiased estimate of the oracle loss using
true labels. Then, as there are no ready training data for label correction, counterfactual labeling is used to
construct artificial training data. Furthermore, since counterfactual labeling utilizes only partial training data, we
design an embedding-based alternative training method to enhance performance. Comparative experiments on both public and
private datasets and detailed analyses show that our proposed approach effectively alleviates the delayed feedback
problem and consistently outperforms the previous state-of-the-art methods.

A preprint version of our paper is available at http://arxiv.org/abs/2307.12756.

# [Dataset](#contents)

- [Criteo dataset](https://drive.google.com/file/d/1x4KktfZtls9QjNdFYKCjTpfjM4tG2PcK/view?usp=sharing)

# [Environment Requirements](#contents)

- Hardware（CPU）
    - Prepare hardware environment with GPU processor.
- Framework
    - [MindSpore-2.0.0](https://www.mindspore.cn/install/en)

- Requirements

```shell

       $ conda create --name <env> --file requirements.txt

```

- For more information, please check the resources below：
    - [MindSpore Tutorials](https://www.mindspore.cn/tutorials/en/r2.0/index.html)
    - [MindSpore Python API](https://www.mindspore.cn/docs/en/r2.0/index.html)

# [Quick Start](#contents)

After installing MindSpore via the official website, you can start training and evaluation as follows:

- processing dataset

# [Script Description](#contents)

## [Script and Sample Code](#contents)

```bash
.
└─ULC
  └─src
      ├─alternate_train.py          # modules in ULC
      ├─data.py                     # data process  
      ├─loss.py                     # loss in ULC
      ├─main.py                     # train ULC
      ├─metric.py                   # metrics in ULC
      ├─models.py                   # ULC structure
      └─utils.py                    # modules in ULC
```

## [Training Process](#contents)

### Training

- running on CPU

  ```python
    python ./src/main.py --method ULC --l2_reg 0.00001 --cuda_device 0 --lr 0.0001 --CD 7 --batch_size 1024 --optimizer Adam --seed 0
  ```

# [ModelZoo Homepage](#contents)

 Please check the official [homepage](https://gitee.com/mindspore/models)
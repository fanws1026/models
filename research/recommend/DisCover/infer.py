# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================

import argparse
import numpy as np
import mindspore as ms
from mindspore import dataset as ds
import models
from models import PitchEncoder, SpeakerEncoder, DisCover
from utils.cqt_loader import CQTpair
from utils.utility import calc_mean_avg_precision, norm
from tqdm import tqdm

ms.set_context(device_target="GPU", device_id=0)


def val(model, dataloader):
    model.set_train(False)
    labels, features = [], []
    for _, (data, label) in tqdm(enumerate(dataloader), total=len(dataloader)):
        feature = model(data, infer=True).asnumpy()
        label = label.asnumpy()
        features.append(feature)
        labels.append(label)
    features = np.squeeze(np.array(features))
    labels = np.squeeze(np.array(labels))
    features = norm(features)
    dis2d = -np.matmul(features, features.T)
    mean_avg_precision, top10, rank1 = calc_mean_avg_precision(dis2d, labels)
    return mean_avg_precision, top10, rank1


def evaluate(args):
    model = getattr(models, args.model)(latent_dim=args.latent_dim)
    f0_encoder = PitchEncoder(latent_dim=args.latent_dim)
    speaker_encoder = SpeakerEncoder(latent_dim=args.latent_dim)
    model_discover = DisCover(model, f0_encoder, speaker_encoder)
    model_discover.load(args.load_model_path)
    model_discover.set_train(False)

    test_data = CQTpair("test", out_length=None)
    test_data80 = CQTpair("songs80", out_length=None)

    test_dataloader = ds.GeneratorDataset(
        test_data,
        shuffle=False,
        num_parallel_workers=1,
        column_names=["query", "label"],
    ).batch(1)
    test_dataloader80 = ds.GeneratorDataset(
        test_data80,
        shuffle=False,
        num_parallel_workers=1,
        column_names=["query", "label"],
    ).batch(1)

    mean_avg_precision, top10, rank1 = val(model_discover, test_dataloader)
    print("SHS100K test: epoch, mean_avg_precision, top10, rank1:")
    print(mean_avg_precision, top10, rank1)
    mean_avg_precision, top10, rank1 = val(model_discover, test_dataloader80)
    print("Covers80 test: epoch, mean_avg_precision, top10, rank1:")
    print(mean_avg_precision, top10, rank1)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="DisCover")
    parser.add_argument("--model", type=str, default="CQTNet", help="model choice")
    parser.add_argument("--load_model_path", type=str, default="", help="")
    parser.add_argument("--latent_dim", type=int, default=300, help="")
    my_args = parser.parse_args()
    evaluate(my_args)

# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
import os
from collections import namedtuple
import mindspore as ms
from mindspore import ops, nn


class BasicCell(nn.Cell):
    def __init__(self):
        super(BasicCell, self).__init__()
        self.model_name = str(type(self))

    def load(self, path):
        param_not_load, _ = ms.load_param_into_net(self, ms.load_checkpoint(path))
        print("param_not_load:", param_not_load)

    def save(self, name=None, ep=None):
        if not os.path.exists("./check_points/"):
            os.mkdir("./check_points/")
        prefix = "./check_points/" + name + "/"
        if not os.path.exists(prefix):
            os.mkdir(prefix)
        name = prefix + str(ep) + ".ckpt"
        print("model name", name.split("/")[-1])
        ms.save_checkpoint(self, prefix + "latest.ckpt")
        return name

    def load_latest(self, notes):
        path = "./check_points/" + notes + "/latest.ckpt"
        param_not_load, _ = ms.load_param_into_net(self, ms.load_checkpoint(path))
        print("param_not_load:", param_not_load)

    def save_best(self, name=None):
        if not os.path.exists("./check_points/"):
            os.mkdir("./check_points/")
        prefix = "./check_points/" + name + "/"
        if not os.path.exists(prefix):
            os.mkdir(prefix)
        name = prefix + "best.ckpt"
        print("model name", name.split("/")[-1])
        ms.save_checkpoint(self, name)
        return name

    def load_best(self, notes):
        path = "./check_points/" + notes + "/best.ckpt"
        param_not_load, _ = ms.load_param_into_net(self, ms.load_checkpoint(path))
        print("param_not_load:", param_not_load)


class FirstStageTrainOneStepCell(nn.Cell):
    def __init__(self, net, optim, cp_mi_net, cs_mi_net, discriminator, args):
        super(FirstStageTrainOneStepCell, self).__init__()
        self.net = net
        self.net.set_grad()
        self.optimizer = optim
        self.weights = self.optimizer.parameters
        self.loss_fn = nn.CrossEntropyLoss()
        self.cp_mi_net = cp_mi_net
        self.cs_mi_net = cs_mi_net
        self.discriminator = discriminator
        self.distance_estimator = nn.MSELoss()
        self.adv_loss = nn.BCELoss()
        self.args = args
        self.output_class = namedtuple('output', ['loss', 'infer_q', 'biased_q', 'pitch', 'speaker'])

    def trans_cost(self, feature4infer_q, feature4infer_t):
        return self.distance_estimator(feature4infer_q, feature4infer_t)

    def forward_fn(self, query, target, label, pitch, speaker, spk_label):
        (
            score_q,
            feature4infer_q,
            spk_score,
            pitch_emb,
            speaker_emb,
            biased_feature_q,
        ) = self.net(query, pitch, speaker)
        score_t, feature4infer_t = self.net(target)
        loss = self.loss_fn(score_q, label) + self.loss_fn(score_t, label) + self.loss_fn(spk_score, spk_label)
        feature_grads = ms.grad(self.trans_cost, grad_position=(0, 1))(
            feature4infer_q, feature4infer_t
        )
        grad_q, grad_t = feature_grads[0], feature_grads[1]
        feature4infer_q *= 1 - nn.Softmax(axis=-1)(grad_q)
        feature4infer_t *= 1 - nn.Softmax(axis=-1)(grad_t)
        align_loss = self.distance_estimator(feature4infer_q, feature4infer_t)
        mi_cs_loss = self.cs_mi_net.mi_est(speaker_emb, feature4infer_q)
        mi_cp_loss = self.cp_mi_net.mi_est(pitch_emb, feature4infer_q)

        d_score_p = self.discriminator(feature4infer_q)
        p_labels = ops.ones(
            label.shape[0],
        )
        d_loss_p = self.adv_loss(d_score_p, p_labels)
        loss += self.args.mi_weight * (mi_cs_loss + mi_cp_loss) + align_loss + d_loss_p
        output = self.output_class(loss, feature4infer_q, biased_feature_q, pitch_emb, speaker_emb)
        return output

    def construct(self, query, target, label, pitch, speaker, spk_label):
        grad_op = ops.value_and_grad(
            self.forward_fn, None, self.optimizer.parameters, has_aux=True
        )
        (
            loss,
            feature4infer_q,
            biased_feature_q,
            pitch_emb,
            speaker_emb,
        ), grads = grad_op(query, target, label, pitch, speaker, spk_label)
        loss = ops.depend(loss, self.optimizer(grads))
        output = self.output_class(loss, feature4infer_q, biased_feature_q, pitch_emb, speaker_emb)
        return output


class SecondStageTrainOneStepCell(nn.Cell):
    def __init__(
        self,
        cp_mi_net,
        cs_mi_net,
        optimizer_cp_mi,
        optimizer_cs_mi,
        discriminator,
        optimizer_adv,
        args,
    ):
        super(SecondStageTrainOneStepCell, self).__init__()
        self.discriminator = discriminator
        self.discriminator.set_grad()
        self.cp_mi_net = cp_mi_net
        self.cs_mi_net = cs_mi_net
        self.cp_mi_net.set_grad()
        self.cs_mi_net.set_grad()
        self.optimizer_cp_mi = optimizer_cp_mi
        self.optimizer_cs_mi = optimizer_cs_mi
        self.optimizer_adv = optimizer_adv
        self.weights_cp_mi = self.optimizer_cp_mi.parameters
        self.weights_cs_mi = self.optimizer_cs_mi.parameters
        self.weights_adv = self.optimizer_adv.parameters
        self.adv_loss = nn.BCELoss()
        self.args = args

    def forward_adv(self, feature4infer_q, biased_feature_q):
        d_score_p = self.discriminator(feature4infer_q)
        p_labels = ops.ones(
            feature4infer_q.shape[0],
        )
        d_loss_p = self.adv_loss(d_score_p, p_labels)

        d_score_n = self.discriminator(biased_feature_q)
        n_labels = ops.zeros(
            feature4infer_q.shape[0],
        )
        d_loss_n = self.adv_loss(d_score_n, n_labels)
        loss_adv = d_loss_n + d_loss_p
        return loss_adv

    def forward_cp_mi(self, feature4infer_q, pitch_emb):
        loss_cp_mi = -self.cp_mi_net.loglikeli(pitch_emb, feature4infer_q)
        return loss_cp_mi

    def forward_cs_mi(self, feature4infer_q, speaker_emb):
        loss_cs_mi = -self.cs_mi_net.loglikeli(speaker_emb, feature4infer_q)
        return loss_cs_mi

    def construct(self, feature4infer_q, biased_feature_q, pitch_emb, speaker_emb):
        grad_op_cs_mi = ops.value_and_grad(
            self.forward_cs_mi, None, self.weights_cs_mi, has_aux=False
        )
        loss_cs_mi, grads_cs_mi = grad_op_cs_mi(feature4infer_q, speaker_emb)
        grads_cs_mi = ms.ops.clip_by_value(grads_cs_mi, clip_value_min=-0.1, clip_value_max=0.1)
        loss_cs_mi = ops.depend(loss_cs_mi, self.optimizer_cp_mi(grads_cs_mi))

        grad_op_cp_mi = ops.value_and_grad(
            self.forward_cp_mi, None, self.weights_cp_mi, has_aux=False
        )
        loss_cp_mi, grads_cp_mi = grad_op_cp_mi(feature4infer_q, pitch_emb)
        grads_cp_mi = ms.ops.clip_by_value(grads_cs_mi, clip_value_min=-0.1, clip_value_max=0.1)
        loss_cp_mi = ops.depend(loss_cp_mi, self.optimizer_cs_mi(grads_cp_mi))

        grad_op_adv = ops.value_and_grad(
            self.forward_adv, None, self.weights_adv, has_aux=False
        )
        loss_adv, grads_adv = grad_op_adv(feature4infer_q, biased_feature_q)
        loss_adv = ops.depend(loss_adv, self.optimizer_adv(grads_adv))
        return loss_cp_mi, loss_cs_mi, loss_adv

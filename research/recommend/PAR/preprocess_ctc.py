# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
import json
from collections import defaultdict
import random
import pickle as pkl
import os
import csv
from datetime import datetime
import stat

import numpy as np

FLAGS = os.O_WRONLY | os.O_CREAT | os.O_EXCL  # 注意根据具体业务的需要设置文件读写方式
MODES = stat.S_IWUSR | stat.S_IRUSR  # 注意根据具体业务的需要设置文件权限

CATE_NUM = 4
LIST_LEN = [10, 10, 10, 10]
SAMPLE_POS_NUM = 3
FILTER_NUM = 30
MAX_OVERLAP_NUM = 2
MAX_SAMPLE = 20
MIN_SAMPLE = 4
PROCESS_NUM = 5
COL_NUM = 1
ROW_NUM = 3
CLICK_ROW_LEN = 10

MAX_HIST_LEN = 10
MIN_HIST_LEN = 2
MIN_THEME = 3
MIN_ITEM = 3
ITM_SPAR_NUM = 5
ITM_DENS_NUM = 128
USR_SPAR_NUM = 1
USR_DENS_NUM = 128


def neg_sample(item_ft_dict, item_embed_dict, theme_item, candi_theme, usr_theme_item,
               item_ft_map, item_map, list_lens):
    theme_num = len(candi_theme)
    page_spar, page_dens, page_label = [[] for _ in range(theme_num)], [[] for _ in range(theme_num)], []
    for idx, theme, list_len in zip(range(theme_num), candi_theme, list_lens):
        sample_pos_num = SAMPLE_POS_NUM
        if len(usr_theme_item[theme]) > sample_pos_num:
            sampled_pos_items = random.sample(usr_theme_item[theme], sample_pos_num)
        else:
            sampled_pos_items = usr_theme_item[theme]
        neg_num = min(list_len - len(sampled_pos_items), len(theme_item[theme]))
        neg_items = random.sample(theme_item[theme], neg_num)
        list_items = list(sampled_pos_items) + list(neg_items)
        list_label = [1 for _ in range(len(sampled_pos_items))] + [0 for _ in range(neg_num)]
        list_zip = list(zip(list_items, list_label))
        random.shuffle(list_zip)
        list_items[:], list_label[:] = zip(*list_zip)
        for item in list_items:
            tmp_ft = [item_ft_map[i][x] for i, x in enumerate(item_ft_dict[item])]
            page_spar[idx].append([item_map[item]] + tmp_ft)
            if item in item_embed_dict:
                page_dens[idx].append(item_embed_dict[item])
            else:
                page_dens[idx].append([0.5 for _ in range(ITM_DENS_NUM)])
        if len(list_items) < list_len:
            page_spar[idx].extend([[0 for _ in range(ITM_SPAR_NUM)] for _ in range(list_len - len(list_items))])
            page_dens[idx].extend([[0 for _ in range(ITM_DENS_NUM)] for _ in range(list_len - len(list_items))])
            list_label.extend([0 for _ in range(list_len - len(list_items))])
        page_label.append(list_label)

    return page_spar, page_dens, page_label


def get_theme_click_date(path):
    print('-----------process theme click------------')
    print('MIN THEME', MIN_THEME, 'MIN ITEM', MIN_ITEM)
    user_item, user_theme, item_ft_dict = defaultdict(set), defaultdict(set), {}
    leaf_cate_set, top_cate_set = set(), set()
    csv_reader = csv.reader(open(path, 'r'))
    _ = next(csv_reader)
    for line in csv_reader:
        user, theme, item, leaf_cate, top_cate, clk_num, _ = line
        user_theme[user].add(theme)
        user_item[user].add(item)
        item_ft_dict[item] = [theme, leaf_cate, top_cate, clk_num]
        leaf_cate_set.add(leaf_cate)
        top_cate_set.add(top_cate)
    print('Theme Click Log: user num', len(user_item), 'item num', len(item_ft_dict),
          'leaf cate num', len(leaf_cate_set), 'top cate num', len(top_cate_set))
    user_theme_1, user_theme_2, user_theme_3 = 0, 0, 0
    user_item_2, user_item_3, user_item_4, user_item_5 = 0, 0, 0, 0
    theme_total_num, item_total_num = 0, 0
    user_set = user_item.keys()
    remove_user = []
    max_theme, max_item = 0, 0
    for user in user_set:
        user_theme_num = len(user_theme[user])
        user_item_num = len(user_item[user])
        max_theme = max(max_theme, user_theme_num)
        max_item = max(max_item, user_item_num)
        theme_total_num += user_theme_num
        item_total_num += user_item_num
        if user_theme_num > 1:
            user_theme_1 += 1
            if user_theme_num > 2:
                user_theme_2 += 1
                if user_theme_num > 3:
                    user_theme_3 += 1
        if user_item_num > 2:
            user_item_2 += 1
            if user_item_num > 3:
                user_item_3 += 1
                if user_item_num > 4:
                    user_item_4 += 1
                    if user_item_num > 5:
                        user_item_5 += 1
        if user_theme_num < MIN_THEME:
            remove_user.append(user)
    print('theme per usr > 1(*): ', user_theme_1 / theme_total_num, '>2:',
          user_theme_2 / theme_total_num, '>3:', user_theme_3 / theme_total_num)
    print('item per user > 2:', user_item_2 / item_total_num, '>3:', user_item_3 / item_total_num,
          '>4:', user_item_4 / item_total_num, '>5:', user_item_4 / item_total_num)
    print('user with max theme', max_theme, 'average theme per user:', theme_total_num / len(user_theme))
    print('user with max item', max_item, 'average item per user:', item_total_num / len(user_item))
    for user in remove_user:
        user_theme.pop(user, 0)
        user_item.pop(user, 0)
    return user_theme, user_item, item_ft_dict


def get_user_hist_data(path):
    print('-----------process user purchase log------------')
    print('MIN HIST LEN', MIN_HIST_LEN, 'MAX HIST LEN', MAX_HIST_LEN)
    user_hist_dict = defaultdict(list)
    item_set = set()
    csv_reader = csv.reader(open(path, 'r'))
    _ = next(csv_reader)
    for line in csv_reader:
        user, item, ts = line
        user_hist_dict[user].append([item, datetime.strptime(ts, '%Y-%m-%d %H:%M:%S')])

    hist_len_2, hist_len_5, hist_len_10, hist_len_40 = 0, 0, 0, 0
    total_hist_len, max_hist = 0, 0
    remove_user = []

    for user, hist in user_hist_dict.items():
        hist_len = len(hist)
        max_hist = max(max_hist, hist_len)
        total_hist_len += hist_len
        if hist_len > 2:
            hist_len_2 += 1
            if hist_len > 5:
                hist_len_5 += 1
                if hist_len > 10:
                    hist_len_10 += 1
                    if hist_len > 40:
                        hist_len_40 += 1
        if len(hist) < MIN_HIST_LEN:
            remove_user.append(user)
        else:
            filtered_hist = sorted(hist, key=lambda k: k[-1], reverse=True)[:MAX_HIST_LEN]
            filtered_hist = [item[0] for item in filtered_hist]
            item_set.update(filtered_hist)
            user_hist_dict[user] = filtered_hist
    print('hist len >2(*):', hist_len_2 / total_hist_len, '>5:', hist_len_5 / total_hist_len,
          '>10:', hist_len_10 / total_hist_len, '>40:', hist_len_40 / total_hist_len)
    print('average hist len:', total_hist_len / len(user_hist_dict))
    print('max hist of user', max_hist, 'item in hist:', len(item_set))
    for user in remove_user:
        user_hist_dict.pop(user, 0)
    return user_hist_dict


def filter_data(theme_clicks_path, user_hist_path):
    user_theme, user_item, item_ft_dict = get_theme_click_date(theme_clicks_path)
    user_hist_dict = get_user_hist_data(user_hist_path)
    print('-----------filter------------')
    user_set = set(user_hist_dict.keys())
    filtered_user_set = user_set.intersection(set(user_theme.keys()))
    union_user_set = user_set.union(set(user_theme.keys()))

    item_set = set(item_ft_dict.keys())
    hist_item_set = set()
    for user in union_user_set:
        if user in filtered_user_set:
            for i in user_item[user]:
                item_set.discard(i)
            for i in user_hist_dict[user]:
                hist_item_set.add(i)
        else:
            user_hist_dict.pop(user, 0)
            user_theme.pop(user, 0)
            user_item.pop(user, 0)
    for item in item_set:
        item_ft_dict.pop(item, 0)

    theme_item = defaultdict(set)
    for item, ft in item_ft_dict.items():
        theme_item[ft[0]].add(item)

    theme_item_10, theme_item_50, theme_item_100, total_theme_item = 0, 0, 0, 0
    for _, items in theme_item.items():
        theme_item_len = len(items)
        total_theme_item += theme_item_len
        if theme_item_len > 10:
            theme_item_10 += 1
            if theme_item_len > 50:
                theme_item_50 += 1
                if theme_item_len > 100:
                    theme_item_100 += 1

    print('after filter, user num', len(filtered_user_set), 'theme clicked item num',
          len(item_ft_dict), 'num theme', len(theme_item))
    print(len(user_theme), len(user_item), len(user_hist_dict))
    print('item per theme > 10:', theme_item_10, '>50:', theme_item_50, '>100:', theme_item_100)
    print('average item per theme', total_theme_item/len(theme_item))
    hist_item_set.update(item_ft_dict.keys())
    data = {
        'user_hist_dict': user_hist_dict,
        'user_theme': user_theme,
        'user_item': user_item,
        'theme_item': theme_item,
        'item_ft_dict': item_ft_dict,
        'hist_item_set': hist_item_set
    }
    return data


def load_embedding(path, item_set):
    item_embed_dict = {}
    csv_reader = csv.reader(open(path, 'r'))
    _ = next(csv_reader)
    for line in csv_reader:
        item, embed = line
        if item in item_set:
            embed = list(map(float, embed.strip().split(' ')))
            item_embed_dict[item] = embed
    print('item with embed', len(item_embed_dict))
    return item_embed_dict


def get_data_map(user_hist_dict, item_set, item_ft_dict):
    # feature map
    print('------------feature map----------')
    user_map, item_map, item_ft_map = defaultdict(int), defaultdict(int), \
                                      [defaultdict(int) for _ in range(ITM_SPAR_NUM - 1)]
    ft_id = 1
    for user in user_hist_dict.keys():
        user_map[user] = ft_id
        ft_id += 1
    print('user num', ft_id)
    for item in item_set:
        item_map[item] = ft_id
        ft_id += 1
    print('item num', len(item_set))
    for item in item_ft_dict:
        itm_ft = item_ft_dict.get(item)
        for i in range(ITM_SPAR_NUM - 1):
            if not item_ft_map[i].__contains__(itm_ft[i]):
                item_ft_map[i][itm_ft[i]] = ft_id
                ft_id += 1
    print('total feature', ft_id)
    print('item sparse feature')
    for i in range(ITM_SPAR_NUM - 1):
        print(len(item_ft_map[i]), end='  ')
    print()
    data = {
        'user_map': user_map,
        'item_map': item_map,
        'item_ft_map': item_ft_map,
        'ft_id': ft_id
    }
    return data


def process_data(item_embed_path, theme_click_path, hist_click_path, processed_path):
    filter_file = processed_path + 'filter.proce'
    if os.path.isfile(filter_file):
        user_hist_dict, user_theme, user_item, theme_item, item_ft_dict, item_set = pkl.load(open(filter_file, 'rb'))
    else:
        data = filter_data(theme_click_path, hist_click_path)
        user_hist_dict = data.get('user_hist_dict')
        user_theme = data.get('user_theme')
        user_item = data.get('user_item')
        theme_item = data.get('theme_item')
        item_ft_dict = data.get('item_ft_dict')
        item_set = data.get('item_set')
        with os.fdopen(os.open(filter_file, FLAGS, MODES), 'wb') as fout:
            pkl.dump([user_hist_dict, user_theme, user_item, theme_item, item_ft_dict, item_set], fout)

    data = get_data_map(user_hist_dict, item_set, item_ft_dict)
    user_map = data.get('user_map')
    item_map = data.get('item_map')
    item_ft_map = data.get('item_ft_map')
    ft_id = data.get('ft_id')

    print('----------load embedding----------')
    item_embed_dict = load_embedding(item_embed_path, item_set)

    # page generation
    print('-----------page generation----------')
    user_data = []
    spar_data = []
    dens_data = []
    lb_data = []
    hist_data = []
    hist_len = []
    div_dict_data = []
    theme_set = theme_item.keys()
    page_theme_num = COL_NUM + ROW_NUM

    def get_record(item_lst, theme_lst):
        usr_theme_item = defaultdict(set)
        for item in item_lst:
            theme = item_ft_dict.get(item)[0]
            usr_theme_item.get(theme).add(item)

        spar_lst, dens_lst, lbs = [], [], []

        sample_num = min(MAX_SAMPLE, len(theme_lst) // page_theme_num + MIN_SAMPLE)
        for _ in range(sample_num):
            if len(theme_lst) < page_theme_num:
                sampel_theme = random.sample(theme_set, page_theme_num - len(theme_lst))
                candi_theme = theme_lst + sampel_theme
            else:
                candi_theme = random.sample(theme_lst, page_theme_num)
            spar, dens, lb = neg_sample(item_ft_dict, item_embed_dict, theme_item, candi_theme,
                                        usr_theme_item, item_ft_map, item_map, LIST_LEN)
            dens_lst.append(dens)
            spar_lst.append(spar)
            lbs.append(lb)

        return spar_lst, dens_lst, lbs

    for user, item_lst in user_item.items():
        theme_lst = list(user_theme[user])
        hist = user_hist_dict[user]
        mapped_hist = []
        for v in hist:
            mapped_hist.append(item_map.get(v))
        spar_lst, dens_lst, lbs = get_record(item_lst, theme_lst)
        if len(np.array(spar_lst).shape) < 3:
            print(np.array(spar_lst).shape)
        for spar, dens, lb in zip(spar_lst, dens_lst, lbs):
            user_data.append(user_map.get(user))
            spar_data.append(spar)
            dens_data.append(dens)
            lb_data.append(lb)
            hist_data.append(mapped_hist)
            hist_len.append(len(hist))
            div_dict_data.append([])

    print('finish page generation')
    print('data num', len(user_data))
    # shuffle and split
    print('spar shape', np.array(spar_data).shape, 'dens shape', np.array(dens_data).shape)
    print('lb shape', np.array(lb_data).shape)
    data = list(zip(user_data, spar_data, dens_data, lb_data, hist_data, hist_len, div_dict_data))
    random.shuffle(data)
    data_length = len(data)
    print('The number of page:', data_length, 'page per user:', data_length / len(user_map))

    train_set = list(zip(*data[:int(0.4 * data_length)]))
    valid_set = list(zip(*data[int(0.4 * data_length): int(0.9 * data_length)]))
    test_set = list(zip(*data[int(0.9 * data_length):]))

    stat_info = {'item_num': len(item_map), 'cate_num': len(item_ft_map[2]), 'list_len': LIST_LEN[0],
                 'ft_num': ft_id, 'page_num': data_length, 'list_num': len(LIST_LEN), 'user_num': len(user_map),
                 'itm_spar_fnum': ITM_SPAR_NUM, 'itm_dens_fnum': ITM_DENS_NUM, 'hist_fnum': 1, 'usr_fnum': 1}
    print('stat', stat_info)
    with os.fdopen(os.open(processed_path + 'data.stat', FLAGS, MODES), 'w') as fout:
        stat_info = json.dumps(stat_info)
        fout.write(stat_info)

    for postfix, date in [('train', train_set), ('valid', valid_set),
                          ('test', test_set)]:
        with os.fdopen(os.open(processed_path + 'data.' + postfix, FLAGS, MODES), 'w') as fout:
            pkl.dump(date, fout)
    print(' =============data process done=============')


if __name__ == '__main__':
    PROCESS_PATH = './data/CloudTheme/process/'
    if not os.path.exists(PROCESS_PATH):
        os.mkdir(PROCESS_PATH)
    ITEM_EMBEDDING_PATH = './data/CloudTheme/raw_data/item_embedding.csv'
    THEME_CLICK_LOG_PATH = './data/CloudTheme/raw_data/theme_click_log.csv'
    HISTORY_CLICK_PATH = './data/CloudTheme/raw_data/user_item_purchase_log.csv'
    process_data(ITEM_EMBEDDING_PATH, THEME_CLICK_LOG_PATH, HISTORY_CLICK_PATH, PROCESS_PATH)

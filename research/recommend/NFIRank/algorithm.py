# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the License);
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an AS IS BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
import mindspore as ms


class Model(ms.nn.Cell):
    def __init__(self, config):
        super(Model, self).__init__(auto_prefix=True)

        # get config
        self.step_len = config.step_len
        self.embedding_dims = config.embedding_dims
        self.user_info_field_size = config.user_info_field_size
        self.context_info_field_size = config.context_info_field_size
        self.video_field_size = config.video_field_size
        self.news_field_size = config.news_field_size
        self.user_info_field_shapes = config.user_info_field_shapes
        self.context_info_field_shapes = config.context_info_field_shapes
        self.video_field_shapes = config.video_field_shapes
        self.news_field_shapes = config.news_field_shapes
        self.candidate_actions = ms.Tensor(config.default_action)

        self.user_info_ranges = config.user_info_ranges
        self.conetxt_info_ranges = config.conetxt_info_ranges
        self.video_info_ranges = config.video_info_ranges
        self.news_info_ranges = config.news_info_ranges

        # create embedding layers
        # api: ms.nn.Embedding(vocab_size, embedding_size, use_one_hot=False,
        # embedding_table="normal", dtype=mstype.float32, padding_idx=None)
        self.user_info_embedding_layers = []
        self.context_info_embedding_layers = []
        self.video_embedding_layers = []
        self.news_embedding_layers = []
        self.embedding_layers = ms.nn.CellList([])

        for shape in self.user_info_field_shapes + self.context_info_field_shapes + self.video_field_shapes \
                     + self.news_field_shapes:
            self.embedding_layers.append(ms.nn.Embedding(vocab_size=shape,
                                                         embedding_size=self.embedding_dims))

        # user_info layers
        for i in range(self.user_info_ranges[0], self.user_info_ranges[1]):
            self.user_info_embedding_layers.append(self.embedding_layers[i])

        # context_info layers
        for i in range(self.conetxt_info_ranges[0], self.conetxt_info_ranges[1]):
            self.context_info_embedding_layers.append(self.embedding_layers[i])

        # video_info layers
        for i in range(self.video_info_ranges[0], self.video_info_ranges[1]):
            self.video_embedding_layers.append(self.embedding_layers[i])

        # news_info layers
        for i in range(self.news_info_ranges[0], self.news_info_ranges[1]):
            self.news_embedding_layers.append(self.embedding_layers[i])

        # create RNN cells
        self.video_rnn_cell = ms.nn.GRUCell(input_size=self.embedding_dims,
                                            hidden_size=32)
        self.news_rnn_cell = ms.nn.GRUCell(input_size=self.embedding_dims,
                                           hidden_size=32)
        self.next_video_rnn_cell = ms.nn.GRUCell(input_size=self.embedding_dims,
                                                 hidden_size=32)
        self.next_news_rnn_cell = ms.nn.GRUCell(input_size=self.embedding_dims,
                                                hidden_size=32)

        # create dense layers
        self.v_dense_weights = ms.nn.CellList([ms.nn.Dense(1120, 32, activation="sigmoid"),
                                               ms.nn.Dense(
                                                   32, 16, activation="sigmoid"),
                                               ms.nn.Dense(16, 1, activation=None)])
        self.a_dense_weights = ms.nn.CellList([ms.nn.Dense(320, 32, activation="sigmoid"),
                                               ms.nn.Dense(
                                                   32, 16, activation="sigmoid"),
                                               ms.nn.Dense(16, 1, activation=None)])
        self.next_v_dense_weights = ms.nn.CellList([ms.nn.Dense(1120, 32, activation="sigmoid"),
                                                    ms.nn.Dense(
                                                        32, 16, activation="sigmoid"),
                                                    ms.nn.Dense(16, 1, activation=None)])
        self.next_a_dense_weights = ms.nn.CellList([ms.nn.Dense(320, 32, activation="sigmoid"),
                                                    ms.nn.Dense(
                                                        32, 16, activation="sigmoid"),
                                                    ms.nn.Dense(16, 1, activation=None)])

        # cast op
        self.float_cast_op = ms.ops.Cast()

    def construct(self, inputs):
        """
        Args:
            Common:
                action: int32, [batch, step_len]
                reward: float32, [batch, ]
                done: bool, [batch, ]
            State:
                user_info: float32, [batch, field_size]
                context_info: float32, [batch, field_size]
                video_list: float32, [batch, step_len, field_size]
                video_num: int32, [batch, ]
                news_list: float32, [batch, step_len, field_size]
                news_num: int32, [batch, ]
            Next State:
                next_user_info: float32, [batch, field_size]
                next_context_info: float32, [batch, field_size]
                next_video_list: float32, [batch, step_len, field_size]
                next_video_num: int32, [batch, ]
                next_news_list: float32, [batch, step_len, field_size]
                next_news_num: int32, [batch, ]
        """
        # depart features
        user_info = inputs["user_info"]
        context_info = inputs["context_info"]
        video_list = inputs["video_list"]
        video_num = inputs["video_num"]
        news_list = inputs["news_list"]
        news_num = inputs["news_num"]

        next_user_info = inputs["next_user_info"]
        next_context_info = inputs["next_context_info"]
        next_video_list = inputs["next_video_list"]
        next_video_num = inputs["next_video_num"]
        next_news_list = inputs["next_news_list"]
        next_news_num = inputs["next_news_num"]

        # get context_info embedding (batch*14*32)
        splitted_context_info = ms.ops.split(
            context_info, axis=1, output_num=context_info.shape[1])
        merged_context_info = []
        for layer, info in zip(self.context_info_embedding_layers, splitted_context_info):
            merged_context_info.append(layer(info))
        merged_context_info = ms.ops.concat(merged_context_info, axis=1)
        merged_context_info = ms.ops.reshape(merged_context_info, (-1, 14 * 32))

        splitted_next_context_info = ms.ops.split(
            next_context_info, axis=1, output_num=next_context_info.shape[1])
        merged_next_context_info = []
        for layer, info in zip(self.context_info_embedding_layers, splitted_next_context_info):
            merged_next_context_info.append(layer(info))
        merged_next_context_info = ms.ops.concat(
            merged_next_context_info, axis=1)
        merged_next_context_info = ms.ops.reshape(
            merged_next_context_info, (-1, 14 * 32))

        # get user_info embedding (batch*1*32)
        merged_user_info = self.user_info_embedding_layers[0](user_info)
        merged_user_info = ms.ops.reshape(merged_user_info, (-1, 32))
        merged_next_user_info = self.user_info_embedding_layers[0](
            next_user_info)
        merged_next_user_info = ms.ops.reshape(merged_next_user_info, (-1, 32))

        # get video_list embedding (batch*10*32)
        embedding_video_list = self.video_embedding_layers[0](video_list)
        embedding_next_video_list = self.video_embedding_layers[0](
            next_video_list)

        # get news_list embedding (batch*10*32)
        embedding_news_list = self.news_embedding_layers[0](news_list)
        embedding_next_news_list = self.news_embedding_layers[0](
            next_news_list)

        q_predict = self.q_network(user_info=merged_user_info,
                                   context_info=merged_context_info,
                                   video_state=embedding_video_list,
                                   news_state=embedding_news_list,
                                   video_num=video_num,
                                   news_num=news_num,
                                   v_dense_layers=self.v_dense_weights,
                                   a_dense_layers=self.a_dense_weights,
                                   video_rnn_cell=self.video_rnn_cell,
                                   news_rnn_cell=self.news_rnn_cell)
        q_target = self.q_network(user_info=merged_next_user_info,
                                  context_info=merged_next_context_info,
                                  video_state=embedding_next_video_list,
                                  news_state=embedding_next_news_list,
                                  video_num=next_video_num,
                                  news_num=next_news_num,
                                  v_dense_layers=self.next_v_dense_weights,
                                  a_dense_layers=self.next_a_dense_weights,
                                  video_rnn_cell=self.next_video_rnn_cell,
                                  news_rnn_cell=self.next_news_rnn_cell)
        q_target = ms.ops.stop_gradient(q_target)

        return q_predict, q_target

    def q_network(self, user_info, context_info, video_state, news_state, video_num, news_num, v_dense_layers,
                  a_dense_layers, video_rnn_cell, news_rnn_cell):
        # v network
        reshaped_video_state = ms.ops.reshape(video_state, (-1, 320))
        reshaped_news_state = ms.ops.reshape(news_state, (-1, 320))
        v_input = ms.ops.concat(
            [user_info, context_info, reshaped_video_state, reshaped_news_state], axis=-1)
        for layer in v_dense_layers:
            v_input = layer(v_input)
        v_value = v_input

        # rnn
        video_seq_ht = self.gru_network(
            cell=video_rnn_cell, inputs=video_state)
        news_seq_ht = self.gru_network(cell=news_rnn_cell, inputs=news_state)

        # a network
        whole_action_vector = self.mix_fn(
            video_seq_ht, news_seq_ht, self.candidate_actions)  # batch*40*320
        a_input = whole_action_vector
        for layer in a_dense_layers:
            a_input = layer(a_input)
        a_value = a_input  # batch*40*1
        tiled_v_value = ms.ops.tile(
            ms.ops.expand_dims(v_value, axis=1), (1, 40, 1))
        q_value = tiled_v_value + a_value

        return q_value

    def gru_network(self, cell, inputs):
        # inputs: batch * length * dims
        # convert inputs to： length * batch * dims
        inputs = ms.ops.transpose(inputs, (1, 0, 2))

        outputs = []
        hx = ms.numpy.zeros((256, 32), ms.float32)
        for i in range(10):
            hx = cell(inputs[i], hx)
            outputs.append(hx)

        outputs = ms.ops.concat(outputs, axis=0)
        outputs = ms.ops.reshape(outputs, (10, 256, 32))
        outputs = ms.ops.transpose(outputs, (1, 0, 2))

        return outputs

    def mix_fn(self, video_state, news_state, candidate_actions):
        candidate_num = candidate_actions.shape[0]
        action = ms.ops.expand_dims(candidate_actions, axis=0)
        video_index = 1 - action
        news_index = action
        video_cum_index = ms.ops.cumsum(video_index, axis=2) - 1
        news_cum_index = ms.ops.cumsum(news_index, axis=2) - 1
        video_cum_index_onehot = ms.ops.one_hot(ms.numpy.where(video_cum_index < 0, ms.numpy.zeros_like(
            video_cum_index), video_cum_index), depth=video_state.shape[1], on_value=ms.Tensor(1.0, ms.float32),
                                                off_value=ms.Tensor(0.0, ms.float32))
        news_cum_index_onehot = ms.ops.one_hot(ms.numpy.where(news_cum_index < 0, ms.numpy.zeros_like(
            video_cum_index), video_cum_index), depth=news_state.shape[1], on_value=ms.Tensor(1.0, ms.float32),
                                               off_value=ms.Tensor(0.0, ms.float32))

        cross_fea = ms.ops.matmul(self.float_cast_op(video_cum_index_onehot, ms.float32),
                                  ms.ops.tile(ms.ops.expand_dims(video_state, axis=1),
                                              (1, candidate_num, 1, 1))) * ms.ops.expand_dims(
            self.float_cast_op(video_index, ms.float32), axis=3) + \
                    ms.ops.matmul(self.float_cast_op(news_cum_index_onehot, ms.float32),
                                  ms.ops.tile(ms.ops.expand_dims(news_state, axis=1),
                                              (1, candidate_num, 1, 1))) * ms.ops.expand_dims(
            self.float_cast_op(news_index, ms.float32), axis=3)
        return ms.ops.reshape(cross_fea, (-1, candidate_num, cross_fea.shape[2] * cross_fea.shape[3]))

# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the License);
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an AS IS BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
import numpy as np


class default_config:
    def __init__(self):
        self.step_len = 10
        self.embedding_dims = 32
        self.user_info_field_size = 32
        self.context_info_field_size = 32
        self.video_field_size = 32
        self.news_field_size = 32
        self.user_info_field_shapes = [320000]
        self.context_info_field_shapes = [320, 20, 40, 40] + [320000] * 10
        self.video_field_shapes = [320000]
        self.news_field_shapes = [320000]

        self.user_info_ranges = [0, 1]
        self.conetxt_info_ranges = [1, 15]
        self.video_info_ranges = [15, 16]
        self.news_info_ranges = [16, 17]

        self.gamma = 0.98
        self.reward_decay = 0.25
        self.reward_decay_array = np.array(
            [self.reward_decay ** i for i in range(10)])

        self.default_action = [[1, 0, 1, 0, 1, 0, 0, 0, 0, 0],
                               [1, 0, 1, 0, 0, 1, 0, 0, 0, 0],
                               [1, 0, 1, 0, 0, 0, 1, 0, 0, 0],
                               [1, 0, 1, 0, 0, 0, 0, 1, 0, 0],
                               [1, 0, 1, 0, 0, 0, 0, 0, 1, 0],
                               [1, 0, 1, 0, 0, 0, 0, 0, 0, 1],
                               [1, 0, 0, 1, 0, 1, 0, 0, 0, 0],
                               [1, 0, 0, 1, 0, 0, 1, 0, 0, 0],
                               [1, 0, 0, 1, 0, 0, 0, 1, 0, 0],
                               [1, 0, 0, 1, 0, 0, 0, 0, 1, 0],
                               [1, 0, 0, 1, 0, 0, 0, 0, 0, 1],
                               [1, 0, 0, 0, 1, 0, 1, 0, 0, 0],
                               [1, 0, 0, 0, 1, 0, 0, 1, 0, 0],
                               [1, 0, 0, 0, 1, 0, 0, 0, 1, 0],
                               [1, 0, 0, 0, 1, 0, 0, 0, 0, 1],
                               [1, 0, 0, 0, 0, 1, 0, 1, 0, 0],
                               [1, 0, 0, 0, 0, 1, 0, 0, 1, 0],
                               [1, 0, 0, 0, 0, 1, 0, 0, 0, 1],
                               [1, 0, 0, 0, 0, 0, 1, 0, 1, 0],
                               [1, 0, 0, 0, 0, 0, 1, 0, 0, 1],
                               [1, 0, 0, 0, 0, 0, 0, 1, 0, 1],
                               [0, 1, 0, 1, 0, 1, 0, 0, 0, 0],
                               [0, 1, 0, 1, 0, 0, 1, 0, 0, 0],
                               [0, 1, 0, 1, 0, 0, 0, 1, 0, 0],
                               [0, 1, 0, 1, 0, 0, 0, 0, 1, 0],
                               [0, 1, 0, 1, 0, 0, 0, 0, 0, 1],
                               [0, 1, 0, 0, 1, 0, 1, 0, 0, 0],
                               [0, 1, 0, 0, 1, 0, 0, 1, 0, 0],
                               [0, 1, 0, 0, 1, 0, 0, 0, 1, 0],
                               [0, 1, 0, 0, 1, 0, 0, 0, 0, 1],
                               [0, 1, 0, 0, 0, 1, 0, 1, 0, 0],
                               [0, 1, 0, 0, 0, 1, 0, 0, 1, 0],
                               [0, 1, 0, 0, 0, 1, 0, 0, 0, 1],
                               [0, 1, 0, 0, 0, 0, 1, 0, 1, 0],
                               [0, 1, 0, 0, 0, 0, 1, 0, 0, 1],
                               [0, 1, 0, 0, 0, 0, 0, 1, 0, 1],
                               [0, 0, 1, 0, 1, 0, 1, 0, 0, 0],
                               [0, 0, 1, 0, 1, 0, 0, 1, 0, 0],
                               [0, 0, 1, 0, 1, 0, 0, 0, 1, 0],
                               [0, 0, 1, 0, 1, 0, 0, 0, 0, 1],]
        self.action_set_size = len(self.default_action)
